<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article class="row" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="contact-page">
		<h1 class="grad-border left sans-bold contact-page_heading"><?php the_title(); ?></h1>
    <?php
		the_content();
    ?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
