<?php
/**
 * Template Name: Contact Us Page Template
 *
 * Displays the Contact Us Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		
</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">
			<div class="contact-page">
				<h1 class="grad-border left sans-bold contact-page_heading"><?php the_field('contact_heading'); ?></h1>
				<section  class="membership-cards">	
					<div class="px-md-2 px-lg-2 membership-card">
						<div class="card h-100 bg-grad-1of2 border-0">
							<p class="text-center"><span class="icon-mail-82x82 text-white" ></span></p>

							<h4 class="sans-bold text-center text-white card-title"><?php the_field('card_title_1'); ?></h4>
							<a href="<?php the_field('card_button_link_1'); ?>" class="btn btn-light btn-white"><?php the_field('card_button_label_1'); ?></a>
						</div>
					</div>

					<div class="px-md-2 px-lg-2 membership-card">
						<div class="card h-100 bg-grad-2of2 border-0">
							<p class="text-center"><span class="icon-braingears-82x82 text-white" ></span></p>
							
							<h4 class="sans-bold text-center text-white card-title"><?php the_field('card_title_2'); ?></h4>
							<a href="<?php the_field('card_button_link_2'); ?>" class="btn btn-light btn-white"><?php the_field('card_button_label_2'); ?></a>
						</div>
					</div>
				</section>
				<section class="contact_links col-11 col-md-10">
					<div class="d-flex contact_links_wrapper align-items-center">
						<figure class="d-flex align-items-center justify-content-center contact_links_figure">
							<p class="text-center mb-xl-0"><span class="icon-school-82x82 text-prim"></span></p>
							
						</figure>
						<div>
							<h3 class="sans-bold contact_links_heading"><?php the_field('home_content'); ?></h3>
							<a class="sans-bold contact_links_mail" href="mailto:<?php the_field('mail_id'); ?>"><?php the_field('mail_id'); ?></a>
						</div>
					</div>
					<div class="d-flex contact_links_wrapper align-items-center">
						<figure class="d-flex align-items-center justify-content-center contact_links_figure">
							<p class="text-center mb-xl-0"><span class="icon-pin-82x82 text-prim"></span></p>
							
						</figure>
						<div>
							<h3 class="sans-bold contact_links_heading"><?php the_field('map_heading'); ?></h3>
							<p class="sans contact_links_para"><?php the_field('map_address'); ?></p>
						</div>
					</div>
					<div class="d-flex contact_links_wrapper align-items-center">
						<figure class="d-flex align-items-center justify-content-center contact_links_figure">
							<p class="text-center mb-xl-0"><span class="icon-network-82x82 text-prim"></span></p>
							
						</figure>
						<div>
							<h3 class="sans-bold contact_links_heading"><?php the_field('contact_us_title'); ?></h3>
							<ul class="d-flex pl-0 contact_links_social">
								<li>
									<a href="<?php the_field('social_link_1'); ?>"><img src="<?php the_field('social_icon_1'); ?>" alt=""></a>
								</li>
								<li>
                                    <a href="<?php the_field('social_link_2'); ?>"><img src="<?php the_field('social_icon_2'); ?>" alt=""></a>
								</li>
								<li>
                                    <a href="<?php the_field('social_link_3'); ?>"><img src="<?php the_field('social_icon_3'); ?>" alt=""></a>
								</li>
								<li>
                                    <a href="<?php the_field('social_link_4'); ?>"><img src="<?php the_field('social_icon_4'); ?>" alt=""></a>
								</li>
							</ul>
						</div>
					</div>
				</section>
			</div>
		</article>
	</main>
	
<!-- Main Page : END -->

<?php get_footer(); ?>
