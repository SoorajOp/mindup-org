<?php
/**
* Template Name: Board Members detail Template
*
* Displays the Board Members detail page
*
*/

get_header(); 
?>
<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">

				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">


		 		<section id="expert-section" class="col-12 padded-80  ">
		 			<div class="container-md">
				 		<div class="row pb-4 align-items-center title-back">
				 			<div class="col-12 col-md-4 pb-3 pb-md-0 d-flex justify-content-center">
				 				
				 				<div class="expert-head" style="background-image: url(<?php the_field('profile_image',get_queried_object()); ?>);"></div>
				 			</div>
				 			<div class="col-12 col-md-8">
				 				<h1><?php the_field('profile_name',get_queried_object()); ?></h1>
				 				<p class="mt-4 ml-2"><?php the_field('profile_designation',get_queried_object()); ?></p>

				 			</div>
				 		</div>
				 		<div class="row">
			 			<div class="col-12 col-md-4 pb-5 pb-md-0">
						 <?php $qualifications = get_field('qualifications');?>
						 <?php if($qualifications) { ?>
			 				<p class="expert-side sans-demi mb-3">Expert:</p>
                             <div class="expertise-list">
			 				    <?php the_field('qualifications',get_queried_object()); ?>
                            </div>
							<?php } ?>
			 			</div>
			 			<div class="col-12 col-md-8 pt-3 pt-md-0 expert">
			 				<h4 class="text-prim pb-4">About</h4>
			 				<?php the_field('about',get_queried_object()); ?>
			 			</div>
			 		</div>
			 	</section>




		</article>
	</main>
<!-- Main Page : END -->
<?php get_footer(); ?>
