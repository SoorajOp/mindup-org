$(document).ready(function() {
	
	$('#videoModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var source_url = button.data('url');
		$('#modalVid').html("<source src='"+source_url+"' type='video/mp4' />");
		var video = document.getElementById('modalVid');
		video.src = source_url;
		video.play();
	});


	$('#videoModalClose').on('click',function(){
		$("#videoModal").modal('hide');
	});
	
	$('#videoModal').on('hide.bs.modal', function (event) {
		document.getElementById('modalVid').pause();
		document.getElementById('modalVid').currentTime = 0;
	});
	
	

});