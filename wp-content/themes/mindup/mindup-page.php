<?php
/**
 * Template Name: Default Mindup Page
 * The template for displaying all single posts
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Mindup
 * @since  1.0
 */

get_header();
?>
<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		
</header> 
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
<?php 
		/* Start the Loop */

    while ( have_posts() ) :
    	the_post();
    	get_template_part( 'template-parts/content/content-page' );
    
    	
    endwhile; // End of the loop.
    
?>
	</main>
	

<?php
get_footer();

?>