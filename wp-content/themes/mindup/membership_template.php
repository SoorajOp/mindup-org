<?php
/**
 * Template Name: Membership Page Template
 *
 * Displays the Membership Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">

				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row padded-bottom-80">

			<section class="col-12 padded-80">
				<div class="container-md">
					<div class="row align-content-center">
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('join_community_heading'); ?></h1>
							<h5 class="text-prim pb-4"><?php the_field('community_description'); ?></h5>
						</div>
						<div class="col-12 col-md-7 d-flex flex-column align-self-center ">
							<?php $pricing = array('post_type' => 'pricing','post_status'=>'publish','order' => 'ASC');
							$pricings = new WP_Query($pricing);
							$color=array("member-bar-1","member-bar-2","member-bar-3 bg-magenta");
							if($pricings->have_posts()) : while ($pricings->have_posts()) : $pricings->the_post(); ?>
								<div class="row no-gutters <?php echo $color[array_rand($color)]; ?> align-items-center mb-3">
									<div class="inner-bar-left col-12 col-sm col-md-12 col-lg">
										<p class="text-white text-center text-sm-left text-md-center text-lg-left sans-bold"><?php the_field('pricing_name'); ?></p>
									</div>
									<div class="inner-bar-right col-12 col-sm col-md-12 col-lg">
										<p class="text-center text-sm-right text-md-center text-lg-right"><a href="<?php the_field('pricing_button_link'); ?>" class="btn btn-primary btn-white mx-0"><?php the_field('pricing_button_label'); ?></a></p> 
									</div>
								</div>
								<?php endwhile;
							endif; wp_reset_query(); ?>
						</div>
						<!-- brain asset  -->
						<div class="col-12 col-md-5 d-flex flex-column align-items-center">
							<img class="img-fluid bubble-img" width="412.64px" height="341px" src="<?php the_field('community_image'); ?>">
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80 bg-mint">
				<div class="container-md ">
					<div class="row justify-content-center " >
						<div class="col-12">
							<h4 class="text-center pb-3"><?php the_field('membership_heading'); ?></h4>
							<h5 class="text-center text-sec pt-3 pb-5"><?php the_field('membership_description'); ?></h5>
						</div>
						<div class="col-12 " >
							<div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 justify-content-center h-100" >
								<div class="col">
									<p class="text-center"><span class="icon-checklist-82x82 text-sec">
									</span></p>
									<p class="text-center member-blurb"><?php the_field('benefits_title_1'); ?></p>
								</div>
								<div class="col">
									<p class="text-center"><span class="icon-notebook-82x82 text-sec">
									</span></p>
									<p class="text-center member-blurb"><?php the_field('benefits_title_2'); ?></p>
								</div>
								<div class="col">
									<p class="text-center"><span class="icon-books-82x82 text-sec">
									</span></p>
									<p class="text-center member-blurb"><?php the_field('benefits_title_3'); ?></p>
								</div>
								<div class="col">
									<p class="text-center"><span class="icon-braingears-82x82 text-sec">
									</span></p>
									<p class="text-center member-blurb"><?php the_field('benefits_title_4'); ?></p>
								</div>
								<div class="col">
									<p class="text-center"><span class="icon-lotus-82x82 text-sec">
									</span></p>
									<p class="text-center member-blurb"><?php the_field('benefits_title_5'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80 brain-section brain-mint bg-white ">
				<div class="container-md padded-top-40 ">		
					<div class="row justify-content-center padded-top-80" >
						<div class="col-12 " >
							<div class="row row-cols-1 row-cols-md-3 justify-content-center h-100 member-number-section" >
								<div class="col mb-4">
									<p class="text-center number-container sans-bold text-white"><span class="numbers">1</span></p>
									<h6 class="text-center text-prim"><?php the_field('feature_title_1'); ?></h6>
									<p class="text-center pb-4"><?php the_field('feature_description_1'); ?></p>
								</div>
								<div class="col mb-4">
									<p class="text-center number-container sans-bold text-white"><span class="numbers">2</span></p>
									<h6 class="text-center text-prim"><?php the_field('feature_title_2'); ?></h6>
									<p class="text-center pb-4"><?php the_field('feature_description_2'); ?></p>
								</div>
								<div class="col mb-4">
									<p class="text-center number-container sans-bold text-white"><span class="numbers">3</span></p>
									<h6 class="text-center text-prim"><?php the_field('feature_title_3'); ?></h6>
									<p class="text-center pb-4"><?php the_field('feature_description_3'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-bottom-80">
				<div class="container-md membership-cards">
					<div class="row no-gutters row-cols-1 row-cols-md-2">
					<?php $pricing = array('post_type' => 'pricing','post_status'=>'publish','order' => 'ASC');
						$pricings = new WP_Query($pricing);
						if($pricings->have_posts()) : while ($pricings->have_posts()) : $pricings->the_post(); ?>
						<?php if( $pricings->current_post == $pricings->post_count-1 ) { ?>
						<div class="col col-md-12 mb-4 ">
							<div class="card lone-card px-4 p-md-4 bg-magenta flex-column flex-md-row">
								<div class="col-12 col-md-6 mb-3 mb-md-0 card-header bg-transparent">
									<h4 class="text-center text-white card-title"><?php the_field('pricing_name'); ?></h4>
									<p class="text-center pt-2 "><a href="<?php the_field('pricing_button_link'); ?>" class="btn btn-light btn-white"><?php the_field('pricing_button_label'); ?></a></p>
								</div>
								<div class="col-12 col-md-6 card-body">
									<h5 class="card-title text-md-center text-white pb-3">Exclusive Access To:</h5>
									<?php the_field('pricing_features'); ?>
								</div>
								
							</div>
						</div>
							<?php
						} else {
							$price_color=array("bg-grad-1of2","bg-grad-2of2");
							?>
							<div class="col col-md-6 pr-md-2 pr-lg-3 mb-4">
								<div class="card h-100 <?php echo $color[array_rand($price_color)]; ?> border-0">
									<div class="card-header bg-transparent border-0">
										<h4 class="text-center text-white card-title"><?php the_field('pricing_name'); ?></h4>
										<div class="text-center">
										<img src="<?php the_field('pricing_image'); ?>" class="card-img" height="82" style="width: 82px !important;">
										</div>
									</div>
									<div class="card-body">
										<h5 class="text-center text-white">Access To:</h5>
										<div clas="text-left">
											<?php the_field('pricing_features'); ?>
										</div>
									</div>
									<div class="card-footer text-center bg-transparent border-0">
										<a href="<?php the_field('pricing_button_link'); ?>" class="btn btn-light btn-white"><?php the_field('pricing_button_label'); ?></a>
									</div>
								</div>
							</div>
							<?php
							 } ?>   
						
						<?php endwhile;
                        endif; wp_reset_query(); ?>
 					</div>
				
				</div>
			</section>

			<section class="col-12 contact_links padded-bottom-80">
				<div class="container-md">
					<div class="d-flex contact_links_wrapper align-items-md-center align-items-start">
						<figure class="d-flex align-items-center justify-content-center contact_links_figure">
							<p class="text-center mb-xl-0"><span class="icon-scholarship-82x82 text-prim"></span></p>
						</figure>
						<div>
							<h3 class="sans-bold contact_links_heading"><?php the_field('scholarship_title'); ?></h3>
							<p class="sans contact_links_para"><?php the_field('scholarship_description'); ?></p>
							
						</div>
					</div>
				</div>
			</section>


		</article>
	</main>
<!-- Main Page : END -->

<?php get_footer(); ?>
