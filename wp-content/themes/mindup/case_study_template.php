<?php
/**
 * Template Name: Case Study Page Template
 *
 * Displays the Case Study Page
 */
get_header(); ?>
<!-- Header : BEGIN -->
	<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">
	
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">
		 		<section id="expert-section" class="col-12 padded-80  ">
		 			<div class="container-md">
				 		<div class="row pb-4 align-items-center title-back">
				 			<div class="col-12 col-md-4 col-sm-4 pb-3 pb-md-0 d-flex justify-content-center">
				 				
				 				<div class="expert-head" style="background-image: url(<?php the_field('case_study_image'); ?>);"></div>
				 			</div>
				 			<div class="col-12 col-md-8 col-sm-8 ">
								 <div class="case_heading_title">
				 					<h1><?php the_field('case_study_title'); ?></h1>
								</div>
				 				<a href="<?php the_field('case_study_button_link'); ?>" class="btn btn-secondary btn-grad-1 btn-small mx-1 mt-4"><?php the_field('case_study_button_label'); ?></a>
				 			</div>
				 		</div>
				 		<div class="row">
			 			<div class="col-12 col-md-4 col-sm-4 hidden-xs-div pb-5 pb-md-0">
			 		
			 			</div>
			 			<div class="col-12 col-md-8 col-sm-8  pt-3 pt-md-0 case_study">
							<?php the_field('case_study_long_description'); ?>
			 			</div>
			 		</div>
			 	</section>
		</article>
	</main>
<!-- Main Page : END -->
<?php get_footer(); ?>
