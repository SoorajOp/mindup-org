<?php
/**
 * Template Name: Research Page Template
 *
 * Displays the Research Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">

				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 padded-80">
				<div class="container-md">
					<div class="row align-content-center">
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('research_heading'); ?></h1>
						</div>
						<div class="col-12 col-md-6 d-flex flex-column align-self-center ">
							<h5 class="text-prim pb-4"><?php the_field('research_title'); ?></h5>
							<p  class="sm-body"><?php the_field('research_description'); ?></p>
							
						</div>
						<!-- brain asset  -->
						<div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center justify-content-md-start">
							<img class="img-fluid bubble-img" src="<?php the_field('research_image'); ?>">
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80 bg-magenta">
				<div class="container-md">
					<div class="row justify-content-center">
						<div class="col-12">
							<h4 class="text-white text-center pb-3"><?php the_field('feature_heading'); ?></h4>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-neuroscience-88x88">
							</span></p>
							<p class="text-center text-white lg-body"><?php the_field('feature_title_1'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-mindfulawareness-88x88">
							</span></p>
							<p class="text-center text-white lg-body"><?php the_field('feature_title_2'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-positivepsych-88x88">
							</span></p>
							<p class="text-center text-white lg-body"><?php the_field('feature_title_3'); ?></p>
						</div>
						<div class="col-6 col-md-3">
							<p class="text-white text-center"><span class="icon-socialemolearning-88x88">
							</span></p>
							<p class="text-center text-white lg-body"><?php the_field('feature_title_4'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-top-80">
				<h2 class="text-center grad-border center pb-4"><?php the_field('benefits_heading'); ?></h2>
				<h5 class="text-center padded-bottom-65 text-prim"><?php the_field('benefits_description'); ?></h5>
				<div class="container-md">
					<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 justify-content-center h-100">
						<div class="col">
							<p class="text-center"><span class="icon-heart-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_1'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-brain-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_2'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-chat-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_3'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-bear-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_4'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-spark-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_5'); ?></p>
						</div>
						<div class="col">
							<p class="text-center"><span class="icon-smiley-60x60">
							</span></p>
							<p class="text-center member-blurb"><?php the_field('benefits_title_6'); ?></p>
						</div>
					</div>
					<div class="row padded-top-65">
						<div class="col-12">
							<p class="text-center footnote"><?php the_field('benefits_address'); ?></p>
						</div>
					</div>

				</div>
			</section>

			<section class="col-12 padded-80 bg-yellow">
				<div class="container-md">
					<div class="">
						<div class="col-12">
							<h2 class="grad-border left"><?php the_field('timeline_heading'); ?></h2>
						</div>
						<div class="row new-founder-columns row-cols-1 file-cards row-cols-md-2 row-cols-lg-3 h-100">
						<?php $timeline = array('post_type' => 'research_timeline','post_status'=>'publish','order' => 'ASC');
						$timelines = new WP_Query($timeline);
						if($timelines->have_posts()) : while ($timelines->have_posts()) : $timelines->the_post(); ?>
							<div id="file-card-0" class="col card-col justify-content-center d-flex d-md-block">
								<div class="card h-100">
									<div style="background-image: url(<?php the_field('timeline_image'); ?>);" class="card-img-top" aria-label=""></div>
									<div class="card-body"></div>
																			
									<div class="card-footer">
										<p class="text-center"><a href="<?php the_field('download_file'); ?>"  class="btn btn-primary btn-grad-1" download>Download</a></p>
									</div>
								</div>
							</div>
							<?php endwhile;
                        endif; wp_reset_query(); ?>
						</div>
					</div>
					
				</div>
			</section>

			<section class="col-12 padded-top-80 bg-mint">
				<div class="container-md">
					<div class="row">
						<div class="col-12">
							<h3 class="text-center pb-4"><?php the_field('feedback_heading'); ?></h3>
							<h5 class="text-sec text-center padded-bottom-40"><?php the_field('feedback_description'); ?></h5>
						</div>
						<div class="col-12 d-flex justify-content-center flex-row flex-wrap ven-group" >
							<div class="ven-circle"><div class="ven-text "><p class=" text-center text-white"><?php the_field('feedback_point_1'); ?></p></div></div>
							<div class="ven-circle"><div class="ven-text "><p class=" text-center text-white"><?php the_field('feedback_point_2'); ?></p></div></div>
							<div class="ven-circle"><div class="ven-text "><p class=" text-center text-white"><?php the_field('feedback_point_3'); ?></p></div></div>
							<div class="ven-circle"><div class="ven-text "><p class=" text-center text-white"><?php the_field('feedback_point_4'); ?></p></div></div>
						</div>

					</div>
					<div class="row justify-content-center">
						<div class="col-12 col-lg-11 padded-top-40">
							<p class="text-center footnote"><?php the_field('feedback_address'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-bottom-80 padded-top-65">
				<div class="container-md padded-top-40 padded-bottom-65">
					<div class="">
						<div class="col-12">
							<h2 class="grad-border left pb-5"><?php the_field('learn_more_heading'); ?></h2>
							<h4 class="text-prim mb-0"><?php the_field('our_experts_heading'); ?></h4>
						</div>
						<div class="row new-founder-columns row-cols-1 row-cols-sm-2  video-cards row-cols-md-3 h-100">
						<?php 
							$experts_news_posts = get_field('experts_news_order');
						
							if( $experts_news_posts ): ?>
							
								<?php foreach( $experts_news_posts as $experts_news_post ): ?>
									<div id="video-cards-0" class="col card-col justify-content-center d-flex d-sm-block">
										<div class="card h-100">
											<a href="#videoModal" role="button" class="video-header-container" data-toggle="modal" data-target="#experts<?php echo $experts_news_post->ID ?>" title="MindUP animation FPO">
																				
												<div style="background-image: url(<?php the_field('news_video_thumbnail',$experts_news_post->ID); ?>);" class="card-img-top" aria-label=""></div>
											</a>
											<div class="card-body">
												<p class="card-text bold"><?php the_field('news_content',$experts_news_post->ID); ?></p>
											</div>								
												
											<div class="card-footer">
												<!-- Video Card link -->
												<p class="video-cta"><a href="#experts<?php echo $experts_news_post->ID ?>" role="button" class="video-header-container" data-toggle="modal" data-target="#experts<?php echo $experts_news_post->ID ?>" title="MindUP animation FPO">Watch the Video</a></p>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							
							<?php endif; ?>
						</div>
						<?php 
						$experts_news_posts = get_field('experts_news_order');
						
						if( $experts_news_posts ): ?>
						
						<?php foreach( $experts_news_posts as $experts_news_post ): ?> 
						<div class="modal fade " id="experts<?php echo $experts_news_post->ID ?>" tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
						    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						    	<span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
						    </button>
						  	<div class="modal-dialog modal-xl modal-dialog-centered">					          
						        <div class="modal-content">
						            <div class="modal-body video-body">
						            <!-- popup video -->
						                <div class="embed-responsive embed-responsive-16by9">
											<div class="embed-responsive-item">
												<?php $news_upload_video = get_field('news_video',$experts_news_post->ID);?>
												<?php if($news_upload_video) { ?>
													<video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('news_video_thumbnail',$experts_news_post->ID); ?>" controls>
														<source src="<?php the_field('news_video',$experts_news_post->ID); ?>" type="video/mp4">
													</video>
												<?php } 
												else { ?>
													<iframe class="embed-responsive-item" src="<?php the_field('news_social_video',$experts_news_post->ID); ?>"></iframe>
												<?php } ?>
												<img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
											</div>
						         		</div>
						            </div>
						        </div>
							</div>
						</div>
						<?php endforeach; ?>
							
							<?php endif; ?>
						<div class="col-12 padded-top-40">
							<h4 class="text-prim mb-0"><?php the_field('case_study_heading'); ?></h4>
						</div>
						<div class="row new-founder-columns row-cols-1 row-cols-sm-2  video-cards row-cols-md-3 h-100">
						<?php 
							$case_study_news_posts = get_field('case_study_news_order');
						
							if( $case_study_news_posts ): ?>
							
								<?php foreach( $case_study_news_posts as $case_study_news_post ): ?>
							<div id="video-cards-3" class="col card-col justify-content-center d-flex d-sm-block">
								<div class="card h-100">
									<a href="#videoModal" role="button" class="video-header-container" data-toggle="modal" data-target="#case_study<?php echo $case_study_news_post->ID ?>" title="MindUP animation FPO">
																		
										<div style="background-image: url(<?php the_field('news_video_thumbnail',$case_study_news_post->ID); ?>);" class="card-img-top" aria-label="<?php //echo $card_cta['cta_image_upload_1']['alt'] ?>"></div>
									</a>
									<div class="card-body">
										<p class="card-text bold pb-1"><?php the_field('news_content',$case_study_news_post->ID); ?></p>
										<!-- <p class="card-text">Case Study: London, England</p> -->
									</div>								
										
									<div class="card-footer">
										<!-- Video Card link -->
										<p class="video-cta"><a href="#case_study<?php echo $case_study_news_post->ID ?>" role="button" class="video-header-container" data-toggle="modal" data-target="#case_study<?php echo $case_study_news_post->ID ?>" title="MindUP animation FPO">Watch the Video</a></p>
									</div>
									
								</div>
							</div>
							<?php endforeach; ?>
							
							<?php endif; ?>
						
						</div>
						<?php 
						$case_study_news_posts = get_field('case_study_news_order');
					
						if( $case_study_news_posts ): ?>
					
						<?php foreach( $case_study_news_posts as $case_study_news_post ): ?>
						<div class="modal fade " id="case_study<?php echo $case_study_news_post->ID ?>"	tabindex="-1"  aria-labelledby="videoModalLabel" aria-hidden="true" >
						    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						    	<span class="close-icon icon-remove-34x34 text-white "   aria-hidden="true"></span>
						    </button>
						  	<div class="modal-dialog modal-xl modal-dialog-centered">					          
						        <div class="modal-content">
						            <div class="modal-body video-body">
						            <!-- popup video -->
						                <div class="embed-responsive embed-responsive-16by9">
											<div class="embed-responsive-item">
												<?php $news_upload_video = get_field('news_video',$case_study_news_post->ID);?>
												<?php if($news_upload_video) { ?>
													<video id="modalVid" class="embed-responsive-item" crossorigin="Anonymous" preload="none" poster="<?php the_field('news_video_thumbnail',$case_study_news_post->ID); ?>" controls>
														<source src="<?php the_field('news_video',$case_study_news_post->ID); ?>" type="video/mp4">
													</video>
												<?php } 
												else { ?>
													<iframe class="embed-responsive-item" src="<?php the_field('news_social_video',$case_study_news_post->ID); ?>"></iframe>
												<?php } ?>
												<img src="<?php echo get_template_directory_uri(); ?>/lib/imgs/ui/icon/video-50x50.svg" class="play_icon" alt="">
											</div>
						         		</div>
						            </div>
						        </div>
							</div>
						</div>
						<?php endforeach; ?>
							
						<?php endif; ?>
					</div>
					
				</div>
			</section>

		</article>
	</main>
<!-- Main Page : END -->

<?php get_footer(); ?>
