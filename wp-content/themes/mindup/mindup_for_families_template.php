<?php
/**
 * Template Name: Mindup for Families Page Template
 *
 * Displays the Mindup for Families Page
 */
get_header(); ?>


<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">
				<div class="for-families">
                </div>
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 top-sky-wave bg-sky padded-top-40 padded-bottom-65">
				<div class="container-md">
					<div class="row align-content-center">
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('program_heading'); ?></h1>
						</div>
						<div class="col-12 col-md-6 d-flex flex-column align-self-center ">
							<h5><?php the_field('program_title'); ?></h5>
							<p  class="pt-3 pb-5"><?php the_field('program_description'); ?></p>
							
						</div>
						<!-- brain asset  -->
						<div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center">
							<img class="img-fluid bubble-img" src="<?php the_field('program_image'); ?>">
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80">
				<div class="container-md  ">
					<div class="row justify-content-center padded-bottom-80">
						<div class="col-12 col-lg-6 d-flex flex-column align-self-center">
							<h5><?php the_field('goldie_brain_title'); ?></h5>
							<p class="the-brain-break-help mt-3 sm-body"><?php the_field('goldie_brain_description'); ?></p>
						</div>
						<div class="col-12 col-lg-6 justify-content-center d-flex">
							<?php $goldie_brain_video = get_field('goldie_brain_video');?>
							<?php if($goldie_brain_video) { ?>
								<video  class="border_radius families-video" width="486px" height="273.38px" poster="<?php the_field('goldie_brain_video_thumbnail'); ?>" controls>
									<source src="<?php the_field('goldie_brain_video'); ?>" type="video/mp4">
								</video>
							<?php } 
							else { ?>
								<iframe class="border_radius families-video" width="486px" height="273.38px" src="<?php the_field('goldie_brain_social_video'); ?>"></iframe>
							<?php } ?>
							
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 bg_parent_tip padded-80">
				<h4 class="text-center text-white padded-bottom-40">Parent tips for bringing MindUP lessons and <br> concepts into the home environment:</h4>
				<div class="container-md">
					<div class="row mission-blocks pt-3 h-100 align-items-stretch">
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold parent_tip_header"><?php the_field('benefits_title_1'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_1'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold parent_tip_header"><?php the_field('benefits_title_2'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_2'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold parent_tip_header"><?php the_field('benefits_title_3'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_3'); ?></p>
								
							</div>
						</div>
						<div class="col-12 col-md-6 padded-bottom-40 align-self-stretch d-flex">
							<div class="mission-block bg-white">
								<h5 class="sans-bold parent_tip_header"><?php the_field('benefits_title_4'); ?></h5>
								<p class="sm-body"><?php the_field('benefits_description_4'); ?></p>
								
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80">
				<div class="container-md padded-bottom-65">
					
					<div class="row bg-magenta justify-content-center padded-40 rounded-lg">
						<div class="col-10">
							<h6 class="text-center text-white padded-bottom-40"><?php the_field('join_community_title'); ?></h6>
							<p class="text-center text-white"><?php the_field('join_community_description'); ?></p>
							<p class="text-center"><a class="btn btn-light btn-white" href="<?php the_field('join_button_link'); ?>"><?php the_field('join_button_label'); ?></a></p>
						</div>
					</div>
				</div>
			</section>

		</article>
	</main>
<!-- Main Page : END -->


<?php get_footer(); ?>
