<?php
/**
 * Template Name: Training Opportunities Page Template
 *
 * Displays the Training Opportunities Page
 */
get_header(); ?>

<!-- Header : BEGIN -->
<header class="container-fluid padded-bottom-80 header-pages">
		<div class="row">
			<div class="col-12 justify-content-center d-flex">

				
			</div>
		</div>		
	</header>
<!-- Header : END -->


<!-- Main Page : BEGIN -->
	<main class="container-fluid">
		<article class="row">

			<section class="col-12 padded-80">
				<div class="container-md">
					<div class="row align-content-center">
						<div class="col-12">
							<h1 class="grad-border left"><?php the_field('training_heading'); ?></h1>
						</div>
						<div class="col-12 col-md-6 d-flex flex-column align-self-center ">
							<h5 class="text-prim pb-2"><?php the_field('training_title'); ?></h5>
							<p class="pb-5 sm-body"><?php the_field('training_description'); ?></p>
							
						</div>
						<!-- brain asset  -->
						<div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center ">
							<img class="img-fluid bubble-img" src="<?php the_field('training_image'); ?>">
						</div>
					</div>
				</div>
			</section>

			<section class="col-12 padded-80">				
				<div class="container-md membership-cards icon-heads">
					<div class="row">
						<div class="col-12 padded-bottom-40">
							<h2 class="grad-border left"><?php the_field('benefits_heading'); ?></h2>
						</div>
						<div class="col-12 padded-bottom-40"> 
							<div class="row no-gutters row-cols-1 row-cols-md-3">
								
								<div class="col pr-md-2 pr-lg-3 pb-4">
									<div class="card h-100 bg-grad-1of3 border-0">
										<div class="card-header bg-transparent border-0">
											<p class="text-center"><span class="icon-onlineplatform-82x82">
											</span></p>
											<div class="card-head-container">
												<h4 class="text-center text-white card-title mb-0"><?php the_field('benefits_title_1'); ?></h4>
											</div>
											<p class="text-center text-white sm-body">(<?php the_field('benefits_type_1'); ?>)</p>
											
										</div>
										<div class="card-body">
											<p class="text-white sans-demi sm-body"><?php the_field('benefits_description_1'); ?></p>
										</div>
										<div class="card-footer text-center bg-transparent border-0">
											<a href="<?php the_field('benefits_button_link_1'); ?>" class="btn btn-light btn-white"><?php the_field('benefits_button_label_1'); ?></a>
										</div>
									</div>
								</div>

								<div class="col px-md-2 px-lg-3 pb-4">
									<div class="card h-100 bg-grad-2of3 border-0">
										<div class="card-header bg-transparent border-0">
											<p class="text-center"><span class="icon-virtualtraining-82x82">
											</span></p>
											<div class="card-head-container">
												<h4 class="text-center text-white card-title mb-0"><?php the_field('benefits_title_2'); ?></h4>
											</div>
											<p class="text-center text-white sm-body">(<?php the_field('benefits_type_2'); ?>)</p>
											
										</div>
										<div class="card-body">
											<p class="text-white sans-demi sm-body"><?php the_field('benefits_description_2'); ?></p>
										</div>
										<div class="card-footer text-center bg-transparent border-0">
											<a href="<?php the_field('benefits_button_link_2'); ?>" class="btn btn-light btn-white"><?php the_field('benefits_button_label_2'); ?></a>
										</div>
									</div>
								</div>

								<div class="col pl-md-2 pl-lg-3 pb-4">
									<div class="card h-100 bg-grad-3of3 border-0">
										<div class="card-header bg-transparent border-0">
											<p class="text-center"><span class="icon-hybridtraining-82x82">
											</span></p>
											<div class="card-head-container">
												<h4 class="text-center text-white card-title mb-0"><?php the_field('benefits_title_3'); ?></h4>
											</div>
											<p class="text-center text-white sm-body">(<?php the_field('benefits_type_3'); ?>)</p>
										</div>
										<div class="card-body">
											<p class="text-white sans-demi sm-body"><?php the_field('benefits_description_3'); ?></p>
											
										</div>
										<div class="card-footer text-center bg-transparent border-0">
											<a href="<?php the_field('benefits_button_link_2'); ?>" class="btn btn-light btn-white"><?php the_field('benefits_button_label_3'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="col-12 padded-bottom-80">
				<div class="container-md padded-bottom-65" >
					<div class="row align-content-center">
						<div class="col-12 padded-top-40"> 
							<div class="row no-gutters bg-magenta justify-content-center padded-40 rounded-lg">
								<div class="col-10">
									<h6 class="text-center text-white"><?php the_field('join_community_title'); ?></h6>
									<p class="text-center text-white padding-bottom-40 sm-body"><?php the_field('join_community_description'); ?></p>
									<p class="text-center"><a class="btn btn-light btn-white" href="<?php the_field('join_button_link'); ?>"><?php the_field('join_button_label'); ?></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="col-12 padded-80">
				<div class="container-md ">
					
				</div>
			</section>

		</article>
	</main>
<!-- Main Page : END -->


<?php get_footer(); ?>
