<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Mindup
 * @since 1.0
 */

?>
		<!-- Footer Slider : BEGIN -->
    <div class="bg-grad-full-magenta footer-slider">
      <div>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php $posts = get_posts( array( 'post_type' => 'testimonial','posts_per_page' => '-1','post_status' => 'publish','orderby' => 'date','order' => 'ASC',
              'meta_key'=>'sort',
							'orderby'=>'meta_value_num' ) );?>
            <?php if ( $posts ) : 
              foreach ($posts as $key=>$post) { ?>
              <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $key; ?>" class="<?php if($key==0){echo "active";} ?>"></li>
            <?php } ?>
            <?php endif; ?>
          </ol>
          <div class="carousel-inner">
            <?php $posts = get_posts( array( 'post_type' => 'testimonial','posts_per_page' => '-1','post_status' => 'publish','orderby' => 'date','order' => 'ASC', 'meta_key'=>'sort','orderby'=>'meta_value_num' ) );?>
              <?php if ( $posts ) : 
                foreach ($posts as $key=>$post) { ?>
                  <div class="carousel-item <?php if($key==0){echo "active";} ?>">
                    <div>
                        <p class="text-white text-center slider-description">“<?php the_field('testimonials_quotes', $post->ID); ?>”</p>
                        <p class="text-white text-center slider-author"><?php the_field('quotes_author', $post->ID); ?></p>
                    </div>
                  </div>
              <?php } ?>
            <?php endif; ?>
          </div>
          <a class="carousel-controls carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span> -->

          </a>
          <a class="carousel-controls carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span> -->
          </a>
        </div>
      </div>
    </div>

	
<!-- Footer Slider : END -->

  <footer class="container-fluid bg-mint">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-6 col-xl-4 ">
          <div class="row d-flex justify-content-center">
            <div class="col-12 d-flex justify-content-center">
              <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/lib/imgs/logocard-mindup-for-life@1x.svg">

            </div>
            <div class="col-12">
              <nav class="navbar social-nav navbar-expand justify-content-center d-flex">
                <ul class="navbar-nav ">
                  <li class="nav-item"><a href="#" class="nav-link"><span class="icon-FB-30x30"></span></a></li>
                  <li class="nav-item"><a href="#" class="nav-link"><span class="icon-Insta-30x30"></span></a></li>
                  <li class="nav-item"><a href="#" class="nav-link"><span class="icon-LinkedIn-30x30"></span></a></li>
                  <li class="nav-item"><a href="#" class="nav-link"><span class="icon-Twitter-30x30"></span></a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-6 col-xl-4 mt-4 mt-md-0 justify-content-center align-items-center d-flex flex-column">
          <div class="form-row footer-nav-chunk justify-content-center">
            
            <div class="col col-4 pr-1 ml-1">
              <h6>About</h6>
              <nav class="navbar footer-nav">
              <?php wp_nav_menu(array('menu' => 'Footer 1', 'show_parent' => true, 'menu_class' => 'navbar-nav ')); ?>
              </nav>
            </div>
            <div class="col px-1">
              <h6>Learn More</h6>
              <nav class="navbar footer-nav">
              <?php wp_nav_menu(array('menu' => 'Footer 2', 'show_parent' => true, 'menu_class' => 'navbar-nav ')); ?>

              </nav>
            </div>
            <div class="col pl-1 d-none d-sm-block" style="max-width: 84px;">
              <h6>MindUP</h6>
              <nav class="navbar footer-nav">
              <?php wp_nav_menu(array('menu' => 'Footer 3', 'show_parent' => true, 'menu_class' => 'navbar-nav ')); ?>

              </nav>
            </div>
              
      
            <div class="col-12 py-2  mt-4 mt-sm-0">
              <nav class="navbar navbar-expand button-nav justify-content-center d-flex">
                <ul class="navbar-nav flex-wrap flex-sm-nowrap justify-content-center">
                  <li class="nav-item"><a href="#" class="btn btn-grad-border mx-0"><div class="grad-inside mint log-in">Log In</div></a></li>
                  <li class="nav-item"><a href="#" class="btn btn-secondary btn-grad-1 btn-small ml-1 mr-0 get-started">Get Started</a></li>
                  <li class="nav-item"><a href="donate.php" class="btn btn-light btn-white btn-small ml-3 mr-0 donate">Donate</a></li>
                </ul>
              </nav>
            </div>

          </div>
        </div>
        
      </div>
      <div class="row d-flex ">
        <div class="col-12 col-lg-6 col-xl-12">
          <nav class="navbar navbar-expand justify-content-center d-flex">
            <ul class="navbar-nav copyright-info justify-content-center flex-wrap flex-sm-nowrap">
              <li class="nav-item"><a href="#" class="nav-link">Privacy Policy</a></li>
              <li class="nav-item"><a href="#" class="nav-link">Terms of Service</a></li>
              <li class="nav-item"><span class="navbar-text">&copy;<?php echo date('Y'); ?> MindUP. All Rights Reserved.</span></li>
            </ul>
          </nav>
        </div>
      </div>
  </footer>

  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/vendors/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/vendors/bootstrap/popper.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/scripts.js"></script>


  <script>
       $(document).ready(function(){
        $("li.menu-item-has-children > a").addClass("dropdown-toggle").attr('data-toggle', 'dropdown');
        $(".dropdown-menu li > a").addClass("dropdown-item");
    });
    </script>
    <script>
      $('body').on('hidden.bs.modal', '.modal', function (e) {
    	  $("video").each(function () { this.pause() });
		    $('video').each(function () { this.currentTime = 0 });
        $("iframe").attr("src", $("iframe").attr("src"));
	    });
    </script>
    <script>
      $(document).ready(function(){
        $(document).on("click",".play_icon", function(e){
    		  e.preventDefault();
          $(this).addClass("hide");
          $(this).parent().children('video').addClass("play");
          $(this).parent().children('video').get(0).play();
        });
        $(document).on("click","#modalVid", function(e){
    		  e.preventDefault();
          $(".play_icon").removeClass("hide");
          $(this).removeClass("play");
          $(this).get(0).pause();
        });
        $(document).on("click","#closeVideo", function(e){
    		  e.preventDefault();
          $(".play_icon").removeClass("hide");
          $("#modalVid").removeClass("play");
          $("#modalVid").get(0).pause();
          $("#modalVid").get(0).currentTime = 0;
        });
      });
    </script>
<?php wp_footer(); ?>

</body>
</html>
