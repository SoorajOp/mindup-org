<div class="module_inner">
<?php  if($slides = $module->slideshow) {
		$show_class = (count($slides) == 1) ? 'noslides' : 'enabled';
		if(count($slides) == 1) {
			$slide = reset($slides);
			$hero = format_hero_img($slide->url);
		}
	?>
		<div class="slideshow_area herospot <?php  echo $show_class ?>">
			<?php  foreach($slides as $slide) {
				$caption = $slide->caption;
				if($caption && ($slide_url = get_field('image_hyperlink', $slide->ID)))
					$caption = '<a href="' . $slide_url . '">' . $caption . '</a>';

				$hero = format_hero_img($slide->url);
				?>
				<div class="slide" <?php  echo $hero ?>>
					<?php  if($caption) { ?>
						<div class="slidecaption">
							<div class="centering_box">
								<h2><?php  echo $caption ?></h2>
							</div>
						</div>
					<?php  } ?>
				</div>
			<?php  } ?>
		</div>
		<?php  if(count($slides) > 1) { ?>
			<div class="slide_nav left"><a title="Previous slide" href="#"><? echo get_sink_icon('arrow_left') ?></a></div>
			<div class="slide_nav right"><a title="Next slide" href=""><? echo get_sink_icon('arrow_right') ?></a></div>
		<?php  } ?>
<?php  } ?>
</div>