<?php
$userFunctions = new sinkMindUpUser();
$user = $userFunctions->getUser();
?>

<section class="socialink-framework mindup-userfunctions userfunctions-header mindup-socialink socialink-functions <?php echo $userFunctions->userfunctions_header_classes() ?>">
	<div class="inner">
	<?php	if($user) { ?>
			<p class="userheader_overview">
				<span class="label">Welcome</span>
				<span class="username"><?php echo do_shortcode('[mepr-account-info field="full_name"]'); //echo $user->first_name ." ". $user->last_name; ?></span>
			</p>
			<div class="userheader_more">
				<?php echo $userFunctions->getUserMenu() ?>
				<p class="logout_link"><a href="<?php echo wp_logout_url(get_permalink()); ?>">Log Out</a></p>
			</div>
		<?php }	else {  ?>
			<ul class="menu-logged-out">
			<li><a href="<?php echo $userFunctions->getUserLogIn() ?>">Log In</a></li>
			<li><a href="<?php echo $userFunctions->getUserSignupPage() ?>" class="gradient-default button">Get Started Now</a></li>
			</ul>
		<?php } ?>
	</div>
</section>

