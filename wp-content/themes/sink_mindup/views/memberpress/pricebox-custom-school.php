<?php
?>


<!-- custom membership box : BEGIN -->
<div id="mepr-price-box-81" class="mepr-price-box socialink-extra price-box-signup-special gradient-divided-3">
	<div class="pricebox-header">
		<?php echo is_front_page() ? '<div class="mepr-most-popular">Most Popular</div>' : '' ?>
		<h2>FREE MEMBERSHIPS</h2>
		<h3>Free</h3>
	</div>
	<div class="mepr-price-box-heading">Access To:<br></div>
	<div class="pricebox-inner">
		<ul>
			<li>Activity resource Library with newly launched get started activities</li>
			<li>On demand virtual events</li>
			<li>Training courses for administrators, families, and for your own self-care.</li>
		</ul>
	</div>
	<div class="mepr-price-box-foot pricebox-footer">
		<div class="mepr-price-box-button">
			<a href="https://member.mindup.org/register/member/">Join Now</a>
		</div>
	</div>
</div>

<div id="mepr-price-box-62" class="mepr-price-box socialink-extra price-box-signup-special gradient-divided-3">
	<div class="pricebox-header">
		<?php echo is_front_page() ? '<div class="mepr-most-popular">Most Popular</div>' : '' ?>
		<h2>Individual MEMBERSHIPS</h2>
		<h3>Request a Quote</h3>
	</div>
	<div class="mepr-price-box-heading">Exclusive Access to:<br></div>
	<div class="pricebox-inner">
		<ul>
			<li>MindUP courses and curriculum</li>
			<li>Expert video series</li>
			<li>Brain Break and Breath Resource Library</li>
			<li>Keep Your Mind Up Activity Library</li>
		</ul>
	</div>
	<div class="mepr-price-box-foot pricebox-footer">
		<div class="mepr-price-box-button">
			<a href="https://member.mindup.org/register/paid-member/">Sign Up Now</a>
		</div>
	</div>
</div>

<div class="mepr-price-box socialink-extra price-box-signup-special gradient-divided-3">
	<div class="pricebox-header">
		<?php echo is_front_page() ? '<div class="mepr-most-popular">&nbsp;</div>' : '' ?>
		<h2>School, Group or District</h2>
		<h3>Request a Quote</h3>
	</div>
	<div class="mepr-price-box-heading">Exclusive Access to:<br></div>
	<div class="pricebox-inner">
		<ul>
			<li>MindUP courses and curriculum</li>
			<li>Expert video series</li>
			<li>Brain Break and Breath Resource Library</li>
			<li>Keep Your Mind Up Activity Library</li>
		</ul>
	</div>
	<div class="mepr-price-box-foot pricebox-footer">
		<div class="mepr-price-box-button">
			<a href="<?php echo $this->getRequestQuotePage() ?>">Request a Quote</a>
		</div>
	</div>
</div>
<!-- custom membership box : END -->