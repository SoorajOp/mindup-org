<?php $prefix = get_option('sink_client_mediaoption_prefix') ? get_option('sink_client_mediaoption_prefix') : ''; ?>
<nav class="search_container dismissable">
	<div class="centering_box flexible">
		<form action="<?php bloginfo('url'); ?>" method="get">
			<input value="" title="Search <?php echo get_bloginfo('title') ?>" type="text" name="s" placeholder="Search <?php echo get_bloginfo('title') ?>" class="search_input clearMeFocus"  />
			<input type="hidden" id="searchsubmit" />
		</form>
		<a href="" class="cancel_search icon-<?php echo $prefix ?>-cancel"></a>
	</div>
</nav>