<!-- site/footer-inner.php : BEGIN -->

<footer class="container-fluid bg-mint" style="padding-top: 108px;">
		<div class="row">
			<div class="col-12 col-md-4 col-lg-6 col-xl-4 ">
				<div class="row d-flex justify-content-center">
					<div class="col-12 d-flex justify-content-center">
						<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer-logo">

					</div>
					<div class="col-12">
						<nav class="navbar social-nav navbar-expand justify-content-center d-flex">
							<ul class="navbar-nav ">
								<li class="nav-item" style="list-style: none;"><a href="#" class="nav-link"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/social/FB-black-30x30@2x.png" alt="footer-logo"></a></li>
								<li class="nav-item" style="list-style: none;"><a href="#" class="nav-link"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/social/Insta-black-30x30@2x.png" alt="footer-logo"></a></li>
								<li class="nav-item" style="list-style: none;"><a href="#" class="nav-link"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/social/LinkedIn-black-30x30@2x.png" alt="footer-logo"></li>
								<li class="nav-item" style="list-style: none;"><a href="#" class="nav-link"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/social/Twitter-black-30x30@2x.png" alt="footer-logo"></a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8 col-lg-6 col-xl-4  justify-content-center align-items-center d-flex flex-column">
				<div class="form-row footer-nav-chunk justify-content-center">
					
					<div class="col-4 pr-1">
						 <?php wp_nav_menu(array('menu' => 'Footer 1', 'sub_menu' => true, 'show_parent' => true, 'menu_class' => 'menu-left',)); ?>
					</div>
					<div class="col-4 px-1">
						 <?php wp_nav_menu(array('menu' => 'Footer 2', 'sub_menu' => true, 'show_parent' => true, 'menu_class' => 'menu-left',)); ?>
					</div>
					<div class="col-4 pl-1">
						 <?php wp_nav_menu(array('menu' => 'Footer 3', 'sub_menu' => true, 'show_parent' => true, 'menu_class' => 'menu-left',)); ?>
					</div>
						
					<div class="col-12 py-2">
						<nav class="navbar navbar-expand button-nav justify-content-center d-flex">
							<ul class="navbar-nav">
								<li class="nav-item" style="list-style: none;"><a href="#" class="btn btn-grad-border ml-0"><div class="grad-inside mint">Log In</div></a></li>
								<li class="nav-item" style="list-style: none;"><a href="#" class="btn btn-secondary btn-grad-1 btn-small mx-1">Get Started</a></li>
								<li class="nav-item" style="list-style: none;"><a href="donate.php" class="btn btn-light btn-white btn-small ml-3 mr-1">Donate</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
			
		</div>
		<div class="row d-flex ">
			<div class="col-12 col-lg-6 col-xl-12">
				<nav class="navbar navbar-expand justify-content-center d-flex">
					<ul class="navbar-nav copyright-info">
						<li class="nav-item"><a href="#" class="nav-link">Privacy Policy</a></li>
						<li class="nav-item"><a href="#" class="nav-link">Terms of Service</a></li>
						<li class="nav-item"><span class="navbar-text">&copy;2021 MindUP. All Rights Reserved.</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</footer>

<!-- site/footer-inner.php : END -->