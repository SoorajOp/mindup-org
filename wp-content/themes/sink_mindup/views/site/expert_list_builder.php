<!-- expert list builder : BEGIN -->
<?php
	if( get_field('expertise_header') ) {
		$expert_header = get_field('expertise_header');
		echo '<p class="expert-side sans-demi text-prim">' . $expert_header . '</p>'; ?>
		<!-- expert list : BEGIN -->
		<ul class="expertise-list">
		<?php
			if(have_rows('list_builder')) :
				while(have_rows('list_builder')) : the_row();

					$list_it 	= get_sub_field('expert_list_item');
					// $list_cnt	= count(get_sub_field('expert_list_item'));

					echo '<li>' . $list_it . '</li>';

				endwhile;
			endif; // if(have_rows('list_builder')) ?>

		</ul>
		<!-- expert list : END -->
			
	<?php } // end if( get_field('expertise_header') ) ?>
<!-- expert list builder : END -->