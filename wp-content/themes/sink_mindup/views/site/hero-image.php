<?php
$hero = hero_override_default();
if($hero) {
$header_caption = get_field('header_caption');
$hero_type = get_field('header_type');
if($hero_type == 'image-text') {
     $img = get_field('hero_image');
     ?>
     <div class="header-image-text">
          <div class="header-image-text-inner flexible centering_box">

               <div class="col left textcol">
                    <?php echo get_field('right_text') ?>
               </div>
               <div class="col right imagecol">
                    <img src="<?php echo $img['sizes']['large'] ?>" alt="<?php echo $img['title'] ?>">
               </div>
          </div>
     </div>
<?php }elseif($hero_type =='text-text') { ?>
     <div class="header-image-text header-text-text">
          <div class="header-image-text-inner flexible centering_box">
               <div class="col left imagecol">
                    <?php echo get_field('left_text') ?>
               </div>
               <div class="col right textcol">
                    <?php echo get_field('right_text') ?>
               </div>
          </div>
     </div>
<?php } elseif($hero_type =='embed-text') { ?>
     <div class="herospot main hero-embed">
          <div class="embed-inner">
               <?php echo get_field('embed_text'); ?>
          </div>
          <?php if($header_caption) { ?><div class="hero_caption"><p><?php echo $header_caption ?></p></div><?php } ?>
          <?php if($buttons = get_field('add_hero_buttons')) { ?>
               <div class="hero-buttons fancyfont">
                    <?php foreach($buttons as $button) {
                         if(!$button['button_label'] || !$button['link'])
                              continue;
                         $bgc = $button['background_color'] ? $button['background_color'] : '#FF15D8';
                         ?>
                         <div class="hero-button"><a style="background-color:<?php echo $bgc ?>" href="<?php echo $button['link'] ?>" class="button gradient-default"><?php echo $button['button_label'] ?></a></div>
                    <?php } ?>
               </div>
          <?php } ?>
     </div>
<?php }elseif($hero->type =='image') { ?>
     <div class="herospot main" <?php echo $hero->bgcode ?>>
          <?php if($header_caption) { ?><div class="hero_caption"><p><?php echo $header_caption ?></p></div><?php } ?>
          <?php if($buttons = get_field('add_hero_buttons')) { ?>
               <div class="hero-buttons fancyfont">
                    <?php foreach($buttons as $button) {
                         if(!$button['button_label'] || !$button['link'])
                              continue;
                         $bgc = $button['background_color'] ? $button['background_color'] : '#FF15D8';
                         ?>
                         <div class="hero-button"><a style="background-color:<?php echo $bgc ?>" href="<?php echo $button['link'] ?>" class="button gradient-default"><?php echo $button['button_label'] ?></a></div>
                    <?php } ?>
               </div>
          <?php } ?>
     </div>
<?php } else { ?>
     <div class="herospot main">
          <div class="slideshow_area enabled">
               <?php  foreach($hero->slides as $slide) {
                    $caption = $slide->caption;
                    if($caption && ($slide_url = get_field('image_hyperlink', $slide->ID)))
                         $caption = '<a href="' . $slide_url . '">' . $caption . '</a>';
                    $hero = format_hero_img($slide->url);
                    ?>
                    <div class="slide" <?php  echo $hero ?>>
                         <?php  if($caption) { ?>
                              <div class="slidecaption">
                                   <div class="centering_box">
                                        <h2><?php  echo $caption ?></h2>
                                   </div>
                              </div>
                         <?php  } ?>
                    </div>
               <?php  } ?>
          </div>
          <div class="slide_nav left"><a title="Previous slide" href="#"><?php echo get_sink_icon('arrow_left') ?></a></div>
          <div class="slide_nav right"><a title="Next slide" href=""><?php echo get_sink_icon('arrow_right') ?></a></div>
     </div>
<?php } ?>
<?php } ?>