<?php
if($_POST['submit_form_fields'] == 'Y') {
	foreach($_POST as $key => $value) {
		if($key!="[submit_form_fields]" && ($value!=""))
			set_sinktheme_option($key, $value);
	}	
	echo '<div class="updated"><p><strong>Theme options have been updated.</strong></p></div>';
}
global $wpeditor_settings;
global $options_array;
?>
<div class="wrap socialink_theme_options">
	<h1 class="sink_dashicon dashicons-flag">Welcome to your Social Ink site!</h1>
	<p>Thanks for choosing to work with Social Ink - we hope you love your site as much as we enjoyed making it.</p>
	<h2>Get Working<h2>
	<p>&larr; To take control of your site, simply use the sidebar on the left to edit and add new content.  You can also edit individual pieces of your site from the frontend by using the editing bar that appears to logged-in users.</p>
	<h2>Troubleshooting, Support, and Feature Requests</h2>
	<p>If you have any questions, the fastest way to get support is through our online ticketing system. Please head over to <a href="http://clients.social-ink.net">clients.social-ink.net</a> and open a support ticket.</p>
	<h2>Spread the Word</h2>
	<p>To learn more about Social Ink and our projects, or tell your colleagues about your humble new web designers and developers, please visit our main site at <a href="http://www.social-ink.net">social-ink.net</a>.</p>
	<div style="clear:both"></div>
</div>