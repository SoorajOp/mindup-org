<!-- footer.php : BEGIN -->
            <?php   $footer_bg = get_hero_image_for('option', 'footer_image'); ?>
            <footer id="footer" role="contentinfo" class="herospot" <?php if($footer_bg) echo $footer_bg->bgcode ?>>
                <?php echo get_subfooter() ?>
                <?php get_footer_inner() ?>
            </footer>
          </div> <!-- .container_inner -->
    </div> <!-- #container -->
    
    <div class="modal fade" id="videoModal" tabindex="-1" aria-hidden="true">
        <button type="button" id="videoModalClose" class="close" aria-label="Close">
            <span class="close-icon"  aria-hidden="true"></span>
        </button>
        <div class="modal-dialog modal-lg modal-dialog-centered">
            
            <div class="modal-content">
                <div class="modal-body video-body">
                <!-- popup video -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <div class="embed-responsive-item">
                            <video id="modalVid" crossorigin="Anonymous" controls>
                        
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php wp_footer(); ?>
<!-- footer.php : END -->
</body>
</html>