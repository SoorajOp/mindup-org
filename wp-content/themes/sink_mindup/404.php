<?php get_header(); ?>
<!-- 404.php : BEGIN -->
	<main class="onepage" id="maincontent" role="main">
		<section class="content centering_box">
			<article class="copy notfound404">
				<header class="pageinfo">
					<h1 class="icon-help-circled">Hmmm, something went wrong. We couldn't find that page.</h1>
					<h2>(Web servers call this a 404 error.)</h2>
				</header>
				<div class="text">
					<?php get_notfound_help() ?>
				</div>
			</article>
		</section>
	</main>
<!-- 404.php : END -->
<?php get_footer(); ?>