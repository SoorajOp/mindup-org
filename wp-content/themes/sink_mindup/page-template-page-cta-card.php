<?php
/*
Template Name: CTA Card Page
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();

		if (get_field('cta_card_grouping')){
			$col_add = true;
			$col_split = 'col-12 col-md-6';
		} else {
			$col_add = false;
			$col_split = 'col-12';
		}

 ?>

 <!-- page-template-page-cta-card.php | TEMPLATE: CTA Card Page : BEGIN -->
 <main class="onepage" id="maincontent" role="main">
	 <article <?php post_class('copy') ?>>
		 

		 <!-- CTA Card Page : BEGIN -->
		 	<div class="title-back">
		 		<section class="container-md px-md-0">
			 		<div class="row">
			 			<div class="col-12">
		 			 		<h1><?php the_title(); ?></h1>
		 			 	</div>
			 			
			 			<div class="<?php echo $col_split; ?>">
			 				
			 			
			 				<?php the_content(); ?>
			 				
			 			</div>
			 			<?php if ($col_add == true) : ?>
			 				<div class="col-12 col-md-6">
			 					<!-- this worked adding the cta card -->
			 					<?php get_ctacard(); ?>	
			 				</div>
			 			<?php endif; ?>
			 		</div>
			 	</section>
		 	</div>
		 	<?php	get_cardimporter(); ?>
		 	

		 	<!-- Insight Timer hardcoded shortcodes : BEGIN -->
			 	<div class="no-back">
			 		<section class="container-md px-md-0">
			 			<div class="row">
			 				<div class="col-12">
			 					<h2 class="small-pink">Explore this Guided Audio Brain Break Created in Partnership with Insight Timer</h2>
			 				</div>
				 			<div class="col-12">
				 				<p class="audio-header">A perfect audio brain break to enjoy in the classroom or to enjoy as a family.</p>
				 				<div class="audio-border">
							 		<?php echo do_shortcode("[player id=1456]"); ?>
							 	</div>
							</div>
                            <div class="col-12">
								<p class="text-center text-md-left mb-0"><a href="https://insig.ht/ezc2wA5febb" class="btn btn-primary btn-grad-1">Explore more brain breaks co-created with Insight Timer</a></p>
                            </div>
						</div>
					</section>
				</div>
			<!-- Insight Timer hardcoded shortcodes : END -->

		 	<!-- Chime hardcoded shortcodes : BEGIN -->
			 	<div class="no-back">
			 		<section class="container-md px-md-0">
			 			<div class="row">
			 				<div class="col-12">
			 					<h2 class="small-pink">Use these chimes to help stimulate the brain during a brain break</h2>
			 				</div>
				 			<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 1</p>
				 				<div class="audio-border audio-not-fullwidth">
							 		<?php echo do_shortcode("[player id=1436]"); ?>
							 	</div>
							</div>
							<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 2</p>
				 				<div class="audio-border audio-not-fullwidth">
									<?php echo do_shortcode("[player id=1437]"); ?>
								</div>
							</div>
							<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 3</p>
				 				<div class="audio-border audio-not-fullwidth">
									<?php echo do_shortcode("[player id=1441]"); ?>
								</div>
							</div>
							<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 4</p>
				 				<div class="audio-border audio-not-fullwidth">
								 	<?php echo do_shortcode("[player id=1442]"); ?>
								</div>
							</div>
							<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 5</p>
				 				<div class="audio-border audio-not-fullwidth">
									<?php echo do_shortcode("[player id=1443]"); ?>
								</div>
							</div>
							<div class="col-12 col-md-4">
				 				<p class="audio-header">Chime 6</p>
				 				<div class="audio-border audio-not-fullwidth">
									<?php echo do_shortcode("[player id=1445]"); ?>
								</div>
							</div>
						</div>
					</section>
				</div>
			<!-- Chime hardcoded shortcodes : END -->

		 	<!-- Zenergy Chime hardcoded : BEGIN -->
			 	<div class="no-back mt-0 pt-0">
			 		<section class="container-md px-md-0">
			 			<div class="row new-founder-columns header-cards row-cols-1">
							<div class="col card-col">
								<div class="card ">
									<div class="card-body d-flex flex-column flex-md-row align-items-center">
										<div class="card-img col-5 col-md-3">
											<img class="img-fluid" src="<?php echo get_theme_file_uri() ?>/images/misc-zenergy-chime.jpg" />
										</div>
										<div class="card-content col-12 col-md-9">
											<p class="card-text text-prim sans-bold header text-center text-md-left">Zenergy Chime</p>
											<p class="card-text mb-3">If you would like to purchase a Zenergy Chime for at home or in the classroom, you can do so at <a href="https://www.chimes.com/collections/meditation/products/zenergy-chime-solo-silver?id=33687732289667" class="text-sec sans-demi">Chimes.com</a>.</p>
											<p class="text-center text-md-left mb-0"><a href="https://www.chimes.com/collections/meditation/products/zenergy-chime-solo-silver?id=33687732289667" class="btn btn-primary btn-grad-1">Buy on Chimes.com</a></p>
										</div>
									</div>
									
								</div>
			 				</div>
			 			</div>
			 		</section>
			 	</div>
		 	<!-- Zenergy Chime hardcoded : END -->
	 </article>
	 
</main>
<!-- page-template-page-cta-card.php | TEMPLATE: CTA Card Page : END -->
<?php get_footer(); ?>