<?php
define('SINK_CLIENT', get_bloginfo('title'));
define('SINK_CLIENT_SHORT', "");
define('SINKSITE_DEBUG' , 1);
define('HOME_PAGE_ID' , 1);
define('SINK_CLIENT_OPTIONPAGE' , true);
define('SINK_CLIENT_SLIDESHOW' , false);
define('SINK_CLIENT_CONTENTFLD' , 'field_5c59e38fc02de');	//edit at your own risk.
define('SINK_CLIENT_EXCERPTFLD' , 'field_5c59e38fc034e');	//edit at your own risk.

//CDN
define('CDN_FONTS' , 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;1,400;1,500&family=Special+Elite&display=swap');
define('FONTS_LOCAL' , get_template_directory_uri() . '/assets/fonts/mywebfonts/MyFontsWebfontsKit.css');

define('SINK_CLIENT_APP_BASEURL', get_template_directory_uri() . '/' . SINK_CLIENT_APPNAME);
define('SINK_CLIENT_APP_ASSETURL', SINK_CLIENT_APP_BASEURL . '/assets/');
define('SINK_CLIENT_APP_DEPENDENCY_URL', SINK_CLIENT_APP_BASEURL . '/dependencies/vendor');
define('SINK_CLIENT_APP_COMPONENT_URL', SINK_CLIENT_APP_BASEURL . '/dependencies/components');
define('SINK_CLIENT_APP_BASEDIR', get_theme_root() . '/' . SINK_CLIENT_APPNAME . '/');

define('SINK_CLIENT_MEDIAOPTIONS' , 'twitter,facebook,instagram,tumblr,youtube,flickr,googleplus');
define('SINK_CLIENT_MEDIAOPTIONS_PREFIX', get_option('sink_client_mediaoption_prefix'));

$defaultfourohfour = '<p class="notfound">
	Unfortunately, we couldn\'t find anything for the URL you entered. Sometimes this means content has been moved - other times, it means we\'ve taken it down for editing or moderation purposes.
	</p>
	<p>To find what you need, please try clicking to the homepage and navigating from there, or using our search functionality to search a live index of the site.</p>
	<p>If all fails - please use the Contact us link at the bottom to get in touch to find what you need.</p>
	<p>Thanks for visiting!</p>';

if(!get_option('sinktheme_error404'))
	add_option('sinktheme_error404',$defaultfourohfour );

$options_array = array(
	array(
			'name'		=>	'sinktheme_error404',
			'caption'	=>	'404 Error Text',
			'tip'		=>	'This text will show when a user visits a page that is not found.'
		),
);

$wpeditor_settings = array(
	'textarea_rows'	=> 5,
);

add_image_size( 'hero', 1440, 720, true );
define('SINKFIELDS_EXCLUDE' ,'add_modules,hero_image_call_to_action_label,hero_image_call_to_action_button_url,hero_image,related_content,thumb,sticky_content,sticky_content_overlay_box,subnavigation_menu,archives_blog_post,first_name,last_name, place_on_map');
define('FIELDS_TOPMETA' ,'eventDateTime');
?>