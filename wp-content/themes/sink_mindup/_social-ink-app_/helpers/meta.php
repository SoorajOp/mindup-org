<?php
//TAXONOMIES
function sink_get_taxes($item, $ignore_taxes = false) {
$taxonomies = get_object_taxonomies( $item->post_type, 'objects' );
$out = array();
foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){
	if($ignore_taxes && in_array($taxonomy_slug,$ignore_taxes))
		continue;
	$terms = get_the_terms( $item->ID, $taxonomy_slug );
	if ( !empty( $terms ) ) {
		$obj = new stdclass();
		$obj->tax = $taxonomy_slug;
		$obj->label = $taxonomy->labels->name;
		foreach($terms as $term)
			$obj->terms[] = $term;
		$out[$taxonomy_slug] = $obj;
	}
}
return !empty($out) ? $out : false;
}
//METADATA
function sink_get_metas($item, $top_and_bottom=true, $top=true, $ignore_metas = false) {
$gmaps_key = "AIzaSyDH2j5AZkmg7vcKmtuYmIhRhg7U3Lot3qo";
$ex = get_exclude_flds();
$topmeta = get_topmeta();
if($metas = get_field_objects($item)) {

	$parsed = array();
	foreach($metas as $slug => $meta) {
		if($ignore_metas && in_array($slug,$ignore_metas))
			continue;
		if(!$meta['value'])
			continue;
		elseif(in_array($slug, $ex))
			continue;
		elseif(!$top_and_bottom && $top && !in_array($slug, $topmeta))
			continue;
		elseif(!$top_and_bottom && !$top && in_array($slug, $topmeta))
			continue;
		//dump($meta);
		$date_format = "l, M d, Y";
		$date_time_format = "l, M d Y g:i a";
		$itm = new stdclass();
		$itm->slug = $slug;
		$itm->label = $meta['label'];
		if($meta['type'] == 'post_object') {
			$val = '<a title="Click to visit ' . $meta['value']->post_title . '" href="' . get_permalink($meta['value']) . '">' . $meta['value']->post_title . '</a>';
		}elseif(($meta['type'] == 'file') && !empty( $meta['value'] ) ){
			$val = '<a title="Click to download" href="' . $meta['value']['url'] . '">' . $meta['value']['title'] . '</a>';
		}elseif( (($meta['type'] == 'url') && !empty( $meta['value'] ) ) || (($meta['type'] == 'text') && (strpos( $meta['value'] , 'http') === 0 ) ) ){
			$val = '<a href="' . $meta['value'] . '">' . $meta['value'] . '</a>';
		}elseif( (($meta['type'] == 'image') && !empty( $meta['value'] ) ) ){
			$val = '<img alt="' . $meta['value']['title'] . '"  title="' . $meta['value']['title'] . '" src="' . $meta['value']['sizes']['medium'] . '" />';
		} elseif($meta['type'] == 'date_picker')
			$val = date($date_format, strtotime($meta['value']));
		elseif($meta['type'] == 'google_map') {
			if($gmaps_key) {
				$val = '
				<iframe
				  width="350"
				  height="350"
				  frameborder="0" style="border:0"
				  src="https://www.google.com/maps/embed/v1/place?key=$' . $gmaps_key . '
				    &q='.$meta['value']['address'].'" allowfullscreen>
				</iframe>
				';
			} else {
				$val .= '<a href="https://maps.google.com?q=' . $meta['value']['address'] . '">' . $meta['value']['address'] . '</a>';
			}
		}
		elseif($meta['type'] == 'email')
			$val = '<a href="mailto:' . $meta['value'] . '">' . $meta['value'] . '</a>';
		else
			$val = $meta['value'];


		if(is_array($val)) {
		}else {
			$itm->val = trim($val);
		}
		if($itm->val)
			$parsed[$slug] = $itm;
	}
}
return !empty($parsed) ? $parsed : false;
}
function get_exclude_flds($ext = false) {
$ex =  explode(',',SINKFIELDS_EXCLUDE);
if(is_array($ex))
	foreach($ex as $idx => $item)
		$ex[$idx] = trim($item);
return $ex;
}
function get_topmeta($ext = false) {
$ex =  explode(',',FIELDS_TOPMETA);
if(is_array($ex))
	foreach($ex as $idx => $item)
		$ex[$idx] = trim($item);
return $ex;
}
//METADATA VIEWS
function getlinklist($cf, $item = false, $flattened = true) {
if(!$item) {
	global $post;
	$item = $post;
}
$css = $flattened ? 'flattened' : 'normal';
if($vals = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_linklist">
		<ul class="<?php echo $css ?>">
			<?php	foreach($vals as $val) { ?>
				<li><a title="Click to read more: <?php echo $val->post_title ?>" href="<?php echo get_permalink($val) ?>"><?php echo $val->post_title ?></a></li>
			<?php } ?>
		</ul>
	</div>
<?php }
return ob_get_clean();
}
function getdivlist($cf, $item = false, $flattened = true, $thumbsize = 'medium') {
if(!$item) {
	global $post;
	$item = $post;
}
if($vals = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_divlist">
		<?php	foreach($vals as $i => $val) {
				$feat_img = (get_the_post_thumbnail($val->ID)!='') ? wp_get_attachment_image_src( get_post_thumbnail_id($val->ID), $thumbsize) : false;
				$first = ($i == 0) ? 'first' : '';
				$last = ($i == ( count($vals) - 1)) ? 'first' : '';
			?>
			<div class="item item<?php echo $i ?> <?php echo $first ?> <?php echo $last ?>">
			<?php if($feat_img) { ?>
				<div class="featimg justify-content-center">
					<a href="<?php echo get_permalink($val->ID) ?>" rel="bookmark" title="Permanent Link to <?php echo get_the_title($val->ID); ?>"><img src="<?php echo $feat_img[0] ?>" alt="<?php echo get_the_title($val->ID) ?>"/></a>
				</div>
			<?php } ?>
				<a title="Click to read more: <?php echo $val->post_title ?>" href="<?php echo get_permalink($val) ?>"><?php echo $val->post_title ?></a>
			</div>
		<?php } ?>
	</div>
<?php }
return ob_get_clean();
}
function make_repeater_tabs($cf, $item = false, $flattened = true, $icon_prefix = "") {
if(!$item) {
	global $post;
	$item = $post;
}
if($vals = get_field($cf, $item)) {
	ob_start();
	?>
	<style type="text/css">
		.tab {
			display:none;
		}
		.tab.active {
			display:block;
		}
	</style>
	<script type='text/javascript'>
	/* <![CDATA[ */
	jQuery(document).ready(function($) {
		$('.fields_repeater_tabs').each(function(i) {
			var $tabsection = $(this);
			var $tab_choices = $tabsection.find(".tabs-menu a").not('.tab_escape').click(function(event) {
				event.preventDefault();
				$(this).addClass("active");
				$(this).siblings().removeClass("active");
				var tab = $(this).attr("href");
				var $alltabs = $tabsection.find(".tab").not(tab).css("display", "none");
				$(tab).fadeIn();
			  });
		});
	});
	/* ]]> */
	</script>
	<div class="sink_cfields fields_repeater_tabs">
			<nav class="tabs-menu">
				<div class="tabs-menu-inner flexible">
					<?php foreach($vals as $i => $val) {
							$first = ($i == 0) ? 'first' : '';
							$active = ($i == 0) ? 'active' : '';
							$last = ($i == ( count($vals) - 1)) ? 'last' : '';
							$val = json_decode(json_encode($val),false);
							$icon = $icon_prefix . $val->icon;

					?>
						<a class="<?php echo $first ?> <?php echo $last ?> <?php echo $active ?>" href="#<?php echo sanitize_title($val->title) ?>"><?php echo $val->title ?><span class="indicator <?php echo $icon ?>"></span></a>
					<?php } ?>
				</div>
			</nav>
			<div class="tab-content">
				<?php foreach($vals as $i => $val) {
						$first = ($i == 0) ? 'first' : '';
						$active = ($i == 0) ? 'active' : '';
						$last = ($i == ( count($vals) - 1)) ? 'last' : '';
						$val = json_decode(json_encode($val),false);
				?>
					<div id="<?php echo sanitize_title($val->title) ?>" class="tab <?php echo $first ?> <?php echo $last ?> <?php echo $active ?>">
						<div class="tab_inner">
							<?php echo $val->content ?>
							<?php if($val->buttons) { ?>
								<div class="blocked">
									<?php foreach($val->buttons as $button) { ?>
										<a href="<?php echo $button->url ?>" class="button <?php echo $button->button_theme ?>"><?php echo $button->button_text ?></a>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			</div>
	</div>
<?php }
return ob_get_clean();
}
function getrepeater($cf, $item = false, $flattened = true) {
if(!$item) {
	global $post;
	$item = $post;
}
if($vals = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_repeater">
		<?php	foreach($vals as $i => $val) {
				$first = ($i == 0) ? 'first' : '';
				$last = ($i == ( count($vals) - 1)) ? 'last' : '';
				$val = json_decode(json_encode($val),false);
			?>
			<div class="item repeating item<?php echo $i ?> <?php echo $first ?> <?php echo $last ?>">
				<span class="prominent"><?php echo $val->label ?></span>
				<span class="subtitle"><?php echo $val->subtitle ?></span>
				<?php if($val->soundcloud_url) { ?>
					<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php echo $val->soundcloud_url ?>&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
					<?php } ?>
			</div>
		<?php } ?>
	</div>
<?php }
return ob_get_clean();
}
function getrepeater_logos($cf, $item = false, $flattened = true) {
if(!$item) {
	global $post;
	$item = $post;
}
if($vals = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_repeater flexible obj-<?php echo $cf ?>">
		<?php	foreach($vals as $i => $val) {
				$first = ($i == 0) ? 'first' : '';
				$last = ($i == ( count($vals) - 1)) ? 'last' : '';
				$val = json_decode(json_encode($val),false);
				$img = '<img src="' .  $val->image->sizes->logos .'" alt="' . $val->image->title  . '" />';
				if($val->url)
					$img = '<a href="' . $val->url . '">' . $img . '</a>';
				//dump($val);
			?>
			<div class="item repeating item<?php echo $i ?> <?php echo $first ?> <?php echo $last ?>">
				<?php echo $img ?>
			</div>
		<?php } ?>
	</div>
<?php }
return ob_get_clean();
}
function get_repeater_indx($cf, $idx, $subfld = false, $item = false) {
if(!$item) {
	global $post;
	$item = $post;
}
if($vals = get_field($cf, $item))
	$item = json_decode(json_encode($vals[$idx]),false);
if($subfld && !empty($item->$subfld))
	return $item->$subfld;
elseif($item)
	return $item;
}
function printmeta($cf, $htmltype = 'p', $item = false, $post_type_restriction = false) {
	if(!$item) {
		global $post;
		$item = $post;
	}
	if($post_type_restriction && ($item->post_type!=$post_type_restriction))
		return false;
	$html = "";
	$val = false;
	$htmltype_open = "<$htmltype>";
	$htmltype_close = "</" . $htmltype . '>';

	if(is_object($item) && property_exists($item,$cf)) {
		$val = $item->$cf;
	} elseif(is_object($item) && ($cf_draw = get_field($cf, $item))) {
		$val = $cf_draw;
	}

	if($val) {
		ob_start();
		?>
		<div class="sink_cfields field_metafield fld-<?php echo sanitize_title($cf) ?>">
			<?php echo $htmltype_open ?><?php echo $val ?><?php echo $htmltype_close ?>
		</div>
	<?php }
	$html = ob_get_clean();
	return $html;
}
function printmeta_btn($btn_label_fld, $btn_url_fld, $item = false, $post_type_restriction = false) {
	if(!$item) {
		global $post;
		$item = $post;
	}
	if($post_type_restriction && ($item->post_type!=$post_type_restriction))
		return false;
	$html = "";
	$btn_label = false;
	$btn_url = false;

	// get label
	if(is_object($item) && property_exists($item,$btn_label_fld)) {
		$btn_label = $item->$btn_label_fld;
	} elseif(is_object($item) && ($cf_draw = get_field($btn_label_fld, $item))) {
		$btn_label = $cf_draw;
	}
	// get URL
	if(is_object($item) && property_exists($item,$btn_url_fld)) {
		$btn_url = $item->$btn_url_fld;
	} elseif(is_object($item) && ($cf_draw = get_field($btn_url_fld, $item))) {
		$btn_url = $cf_draw;
	}

	if($btn_url && $btn_label) {
		ob_start();
		?>
		<div class="sink_cfields field_metafield fld_buttonmade fld-<?php echo sanitize_title($cf) ?>">
			<a class="btn" title="Click to visit" href="<?php echo $btn_url ?>"><?php echo $btn_label ?></a>
		</div>
	<?php }
	$html = ob_get_clean();
	return $html;
}

function printmeta_link($cf, $linklabel = false, $item = false, $post_type_restriction = false) {
	if(!$item) {
		global $post;
		$item = $post;
	}
	if($post_type_restriction && ($item->post_type!=$post_type_restriction))
		return false;
	$html = "";
	$val = false;

	if(is_object($item) && property_exists($item,$cf)) {
		$val = $item->$cf;
	} elseif(is_object($item) && ($cf_draw = get_field($cf, $item))) {
		$val = $cf_draw;
	}

	if($val) {
		ob_start();
		?>
		<div class="sink_cfields field_metafield field_link-field fld-<?php echo sanitize_title($cf) ?>">
			<a href="<?php echo $val ?>"><?php echo $linklabel ?></a>
		</div>
	<?php }
	$html = ob_get_clean();
	return $html;
}
function getwys($cf, $item = false) {
if(!$item) {
	global $post;
	$item = $post;
}
if($val = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_wysiwyg">
		<?php echo $val ?>
	</div>
<?php }
return ob_get_clean();
}
function getimg($cf, $sz = 'medium', $item = false) {
if(!$item) {
	global $post;
	$item = $post;
}
if($val = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_img">
		<img src="<?php echo $val['sizes'][$sz] ?>" alt="">
	</div>
<?php }
return ob_get_clean();
}
function parse_bg_img($cf, $sz = 'medium', $item = false) {
if(!$item) {
	global $post;
	$item = $post;
}
if($val = get_field($cf, $item))
	return 'style="background-image:url(' . $val['sizes'][$sz] . ')"';
}
function getsingle($cf, $item = false) {
if(!$item) {
	global $post;
	$item = $post;
}
if($val = get_field($cf, $item)) {
	ob_start();
	?>
	<div class="sink_cfields field_single_link">
		<a title="Click to read more: <?php echo $val->post_title ?>" href="<?php echo get_permalink($val) ?>"><?php echo $val->post_title ?></a>
	</div>
<?php }
return ob_get_clean();
}