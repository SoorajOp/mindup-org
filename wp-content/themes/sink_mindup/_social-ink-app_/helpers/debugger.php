<?php
/* 	COPYRIGHT SOCIAL INK (C) 2021
	THIS CODE LIBRARY AND FUNCTIONS/SUBROUTINES HEREIN ARE PROPERTY OF SOCIAL INK,
	AND MAY NOT BE REPRODUCED, MODIFIED OR SOLD, EXCEPT AS STIPULATED IN CONTRACT WITH SOCIAL INK.
	NO OTHER DEVELOPERS MAY USE ANY PART OF THE CODE BELOW FOR ANY OTHER PROJECT, OR REUSE THIS CODE
	IN REDEVELOPMENT OF THIS CLIENT'S SITE WITHOUT PERMISSION OR LICENSING FROM SOCIAL INK.
	INFO@SOCIAL-INK.NET
*/
define('SINKDEBUG_NICK' , 'socialink');
define('SINKDEBUG_CONTENTONLY' , false);
// editing

//access
add_filter('body_class', function($classes) {
    global $current_user;

    foreach ($current_user->roles as $user_role) {
        $classes[] = 'role-'. $user_role;
    }

    $classes[] = 'logged-in-user-' . $current_user->ID;

    return $classes;
});
