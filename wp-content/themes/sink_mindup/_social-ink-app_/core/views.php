<?php
function get_subfooter() {include(get_template_directory() . '/views/site/subfooter.php');	}
function get_hero_images() {include(get_template_directory() . '/views/site/hero-image.php');	}
function get_sink_svg($nym) {
	$img = false;
	$imgfile = get_template_directory() . "/images/$nym.svg";
	if(file_exists($imgfile)) {
		ob_start(); ?>
		<span class="sink_img sink_svg">
			<? include($imgfile); ?>
		</span>
		<? $img = ob_get_clean();
	}
	return $img;
}
function get_sink_icon($nym) {
	$icon = false;
	$imgfile = get_template_directory() . "/assets/icons/icon-$nym.svg";
	if(file_exists($imgfile)) {
		ob_start(); ?>
		<span class="sink_icon sink_svg">
			<? include($imgfile); ?>
		</span>
		<? $icon = ob_get_clean();
	}
	return $icon;
}
function socialink_pagebuilder($content_cont = false, $cf = 'add_modules') {
	$pagebuilder_text = '<div class="socialink-modules-extra">';
	if(!$content_cont) {
		global $post;
		$content_cont = $post;
	}
	if($sectionheader = get_field('additional_sections_header', $content_cont)) {
		$pagebuilder_text.= '<header class="mainheader gradient-default"><h1>' . $sectionheader . '</h1></header>';
	}
	if($modules = get_field($cf, $content_cont)) {
		ob_start();
		foreach($modules as $m => $module) {
			$module = json_decode(json_encode($module, true));
			$listview = $module->related_content_type ? $module->related_content_type : false;
			$modfile = get_template_directory() . "/views/builder/$module->module_type.php";
			if(file_exists($modfile)) { ?>
				<section class="socialink-module <? echo $listview ?> module-<? echo $module->module_type ?> module<? echo $m ?>" data-key="<? echo $m ?>">
					<?php  if($module->header) {?>
						<header class="modheader">
							<a name="<?php  echo sanitize_title($module->header) ?>"></a>
							<div class="centering_box">
								<h2 class=""><?php  echo $module->header ?></h2>
							</div>
						</header>
					<?php  } ?>
					<?php include($modfile); ?>
				</section>
			<? } else {
				print_r("ERRORCODE: 5xx000:");
			}
		}
		$pagebuilder_text.= ob_get_clean();
	}
	$pagebuilder_text.= '</div>';
	return $pagebuilder_text;
}
function get_searchbox($hidden = true) {
	if($hidden)
		include(get_template_directory() . '/views/site/searchbox_hidden.php');
	else
		include(get_template_directory() . '/views/site/searchbox.php');
}
function get_header_inner() {include(get_template_directory() . '/views/site/header_inner.php');	}
function get_footer_inner() {include(get_template_directory() . '/views/site/footer_inner.php');	}
function get_notfound_help() {include(get_template_directory() . '/views/site/notfound_404.php'); }
function get_socialmedia() {include(get_template_directory() . '/views/site/socialmedia.php'); }

function get_cardimporter() { include(get_template_directory() . '/views/site/card_importer.php'); }
function get_homecardimporter() { include(get_template_directory() . '/views/site/home_card_importer.php'); }
function get_ctacard() { include(get_template_directory() . '/views/site/cta_card_importer.php'); }
function get_pillarimporter() { include(get_template_directory() . '/views/site/pillar_card_importer.php'); }
function get_expertlist() { include(get_template_directory() . '/views/site/expert_list_builder.php'); }

function get_single_archive($item=false,$i=0, $total=false, $thumbsize='thumbnail', $overwrite_thumb = false) {
	$postfile = get_template_directory() . "/views/lists/$item->post_type.php";
	if(file_exists($postfile))
		include($postfile);
	else
		include(get_template_directory() . '/views/lists/archive.php');
}
function get_single_archive_grid($item=false,$i=0, $total=false, $thumbsize='grid') {
	$postfile = get_template_directory() . "/views/lists/grid_$item->post_type.php";
	if(file_exists($postfile))
		include($postfile);
	else
		include(get_template_directory() . '/views/lists/grid_archive.php');
}
if( function_exists('acf_add_options_page') ) {
	$args = array(
		'position' 		=> 2,
		'page_title' 	=> 'Customize Site',
		'capability'	=>	'edit_posts',
	);
	acf_add_options_page($args);
}