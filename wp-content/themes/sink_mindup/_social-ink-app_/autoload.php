<?php
require_once('config/constants.php');
require_once('dependencies/vendor/autoload.php');
require_once('core/wp.php');
require_once('core/views.php');
require_once('helpers/library.php');
require_once('helpers/debugger.php');
require_once('helpers/user.php');
require_once('helpers/meta.php');
require_once('helpers/memberpress.php');
require_once('helpers/aria-nav-menu-walker.php');

//SCRIPTS
function sink_wpsc_js_scripts()	{
	
	wp_register_script( 'sink-theme-js-slick', SINK_CLIENT_APP_DEPENDENCY_URL . '/kenwheeler/slick/slick/slick.min.js' );
	wp_register_script( 'sink-theme-js-fancybox', SINK_CLIENT_APP_COMPONENT_URL . '/fancybox/source/jquery.fancybox.pack.js' );
	wp_register_script( 'sink-theme-js', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ) );

	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/vendor/bootstrap/bootstrap.min.js', array(), '4.6.0', true);
	wp_enqueue_script( 'popper-js', get_template_directory_uri() . '/assets/js/vendor/bootstrap/popper.min.js', array(), '1.16.1', true );
	wp_enqueue_script( 'sink-theme-js' );
	wp_enqueue_script( 'sink-theme-js-slick' );
}
add_action( 'wp_enqueue_scripts', 'sink_wpsc_js_scripts' );


//CSS & SASS enqueues
function sink_wpsc_css_styles() {
	// bootstrap
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/vendor/bootstrap/bootstrap.css', array(), '', 'all'  );

	wp_register_style( 'sink-theme-fonts', FONTS_LOCAL);
	wp_register_style( 'sink-theme-icons', get_template_directory_uri() . '/assets/icons/css/icons.css', array(), null, 'all' );
	wp_register_style( 'sink-theme-slideshow-slick', SINK_CLIENT_APP_DEPENDENCY_URL . '/kenwheeler/slick/slick/slick.css', array(), null, 'all' );

	// New page Styles
	wp_enqueue_style( 'sink-theme', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

  // all page styles
	//wp_register_style( 'sink-theme-css', get_template_directory_uri() . '/style.css', array(),null, 'all' );
	//wp_register_style( 'sink-theme-print-scss', get_template_directory_uri() . '/sink.style.php/print.scss', array(), null, 'all' );
	//wp_register_style( 'sink-theme-scss', get_template_directory_uri() . '/sink.style.php/style.scss', array(), null, 'all' );
	//wp_register_style( 'sink-theme-res768', get_template_directory_uri() . '/assets/css/responsive_768max.css', array('sink-theme-fonts','sink-theme-scss','sink-theme-css'), null, 'screen and (max-width:767px)' );
	wp_enqueue_style( 'sink-theme-slideshow-slick' );
	//wp_enqueue_style( 'sink-theme-res768' );

}
add_action( 'wp_enqueue_scripts', 'sink_wpsc_css_styles' );