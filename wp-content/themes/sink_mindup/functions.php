<?php
	define('SINK_CLIENT_APPNAME', '_social-ink-app_');
	require_once(SINK_CLIENT_APPNAME . '/autoload.php');
	
	//WP SEO
		add_filter( 'wpseo_use_page_analysis', '__return_false' );
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
	
	//Disable YOAST nag messages
		add_filter( 'ac/suppress_site_wide_notices', '__return_true' );
		if(class_exists('Yoast_Notification_Center')) {
			remove_action( 'admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
			remove_action( 'all_admin_notices', array( Yoast_Notification_Center::get(), 'display_notifications' ) );
		}
		add_action('admin_init', 'cpac_disable_review_notice');
	
	//WP LIB
	add_post_type_support( 'page', 'excerpt' );
	if (function_exists('add_theme_support')) {
		add_theme_support('menus');
		add_theme_support( 'post-thumbnails' );
	}
	add_action('init', 'remheadlink');
	function remheadlink() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}
	function cpac_disable_review_notice() {
	   update_user_meta( get_current_user_id(), 'cpac-hide-review-notice' , '1', true );
	}
	function filter_media_comment_status( $open, $post_id ) {
	    $post = get_post( $post_id );
	    if( $post->post_type == 'attachment' ) {
	        return false;
	    }
	    return $open;
	}
	add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );
	add_action('admin_init', 'cpac_disable_review_notice');
	add_action('init', 'do_output_buffer');	//prevent headers already sent redirects/WP
	function do_output_buffer() {	ob_start();	}
	add_action( 'admin_init', 'socialink_theme_add_editor_styles' );
	function socialink_theme_add_editor_styles() {
		add_editor_style(get_template_directory_uri() . '/assets/css/tinymce.css');
	}

	/*add_action('wp_enqueue_scripts', 'remove_mp_validation');
	function remove_mp_validation() {
		
		wp_deregister_script('mp-validate');
		wp_dequeue_script('mp-validate');
		
		
	}


	function collectiveray_load_js_script() {
	  if( is_single('62') ) {
	    wp_enqueue_script('mp-validate', get_template_directory_uri() . '/assets/js/mp_validate_override.js', array( 'jquery.payment' ),'1.0.0',false );
	    //or use the version below if you know exactly where the file is
	    //wp_enqueue_script( 'js-file', get_template_directory_uri() . '/js/myscript.js');
	  } 
	}

	add_action('wp_enqueue_scripts', 'collectiveray_load_js_script');
*/
	//add_action( 'wp_enqueue_scripts', 'override_mp_validation' );	
	//function override_mp_validation(){
	//	wp_register_script('mp-validate-override',get_template_directory_uri() . '/assets/js/mp_validate_override.js', array( 'jquery' ),'1.0.0',false );
	//	wp_enqueue_scripts('mp-validate-override');
	//}
	
	//homepage redirect
	add_filter( 'pre_option_page_on_front', 'changeHomePage');
	function changeHomePage(){
	
	if ( is_user_logged_in() ) {
		$membershipID_paid = get_page_by_title('Paid Member',OBJECT,'memberpressproduct');
		$membershipID_free = get_page_by_title('Member',OBJECT,'memberpressproduct');
		
		$mepr_user = new MeprUser( get_current_user_id() );
		
		if($mepr_user->is_already_subscribed_to( $membershipID_paid->ID )){
			return 1505;
		} else if($mepr_user->is_already_subscribed_to( $membershipID_free->ID )){
			return 1272;
		} else {
			return 1505;
		}
		//return 1505;
	} else {
		return 106;
	}

}


?>
