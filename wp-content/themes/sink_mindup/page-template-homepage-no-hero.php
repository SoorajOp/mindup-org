<?php
/*
Template Name: Homepage - no hero
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>
<!-- page-template-homepage-no-hero.php | TEMPLATE: Homepage - No Hero : BEGIN -->
<main class="onepage" id="maincontent" role="main">
<section class="content centering_box">
	<article <?php post_class('copy') ?>>
		<div class="text">
			<?php the_content(); ?>
		</div>
	</article>
</section>
<section class="join-section">
	<div class="centering_box">
		<h1>Join Our Amazing Community</h1>
		<p class="description">Membership Options</p>
		<div class="group-plans-embed">
			<? echo do_shortcode('[mepr-group-price-boxes group_id="90"]') ?>
		</div>

		<p class="description"><a href="home/plans/membership-options/" class="gradient-default button">Join Our Online Community</a></p>
		<div class="d-flex justify-content-center">

        	<blockquote class="description col-12 col-md-6">MindUP reinforces the enjoyment and impact of the experience of learning.<br>&mdash; Daniel J. Siegel, MD</blockquote>
    	</div>
        
        <p class="description"><br /><a href="mindup-in-the-news/" class="gradient-default button">MindUP in the News</a></p>

	</div>
</section>



</main>
<!-- page-template-homepage-no-hero.php | TEMPLATE: Homepage - No Hero : END -->
<?php get_footer(); ?>
