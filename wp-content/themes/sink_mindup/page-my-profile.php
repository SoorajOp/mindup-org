<?php get_header(); ?>
<!-- page-my-profile.php : BEGIN -->
<main class="onepage" id="maincontent" role="main">
	<section class="content centering_box">
		<?php if (have_posts()) {
			the_post(); 	?>
			<article <?php post_class('copy') ?>>
				<header class="pageinfo">
					<h1>My Profile</h1>
				</header>
				<div class="text">
					<?php the_content(); ?>
				</div>
			</article>
		<?php } ?>
	</section>
 </main>
<!-- page-my-profile.php : BEGIN -->
<?php get_footer(); ?>