/* ****************
SOCIAL INK JS LIBRARY _ DO NOT REPRODUCE.
********************************************** */
// Avoid `console` errors in browsers that lack a console.
(function() { var e; var d = function() {}; var b = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn"]; var c = b.length; var a = (window.console = window.console || {}); while (c--) { e = b[c]; if (!a[e]) { a[e] = d } } }());
// JQ STARTS BELOW
jQuery(document).ready(function($) {
    console.log('** BEGIN loading Social Ink site');
    var clearMePrevious = "";
    $(".clearMeFocus").focus(function() { if ($(this).val() == $(this).attr("title")) { clearMePrevious = $(this).val();
            $(this).val("") } });
    $(".clearMeFocus").blur(function() { if ($(this).val() == "") { $(this).val(clearMePrevious) } });
    jQuery.fn.exists = function() { return this.length > 0; }
    $('ul li:first-child').addClass('first_item');
    $('ul li:last-child').addClass('last_item');
    $('.mobile_expand').hide();
    $('.mobile_expansion').click(function(e) {
        var $target = $('#' + $(this).data('expand'));
        if ($(this).is(':checked'))
            $target.fadeIn('fast');
        else
            $target.hide();
    });
    if ($('.slideshow_area.enabled').exists()) {
        $('.slideshow_area.enabled').each(function(i) {
            var $show = $(this);
            var $parent = $(this).parent();
            var $right = $parent.find('.slide_nav.right a');
            var $left = $parent.find('.slide_nav.left a');
            $show.slick({
                nextArrow: $right,
                prevArrow: $left,
                arrows: true,
                fade: false,
                speed: 100,
                autoplay: false,
                cssEase: 'linear'
            });
        });
    }
    $('.headersearch').click(function(e) {
        e.preventDefault();
        var $target = $($(this).data('expand'));
        if ($(this).is('.expanded')) {
            hideSearch($target, $(this));
        } else {
            showSearch($target, $(this));
        }
    });
    $('.cancel_search').on('click', function(e) {
        e.preventDefault();
        var $target = $(this).parents('.dismissable');
        hideSearch($target, $('a.headersearch'));
    });

    function showSearch($target, $clicker) {
        $('.topmenu').addClass('searchopen');
        $('.search_container').addClass('searchopen');
        $target.slideDown('fast').find('input[type="text"]').focus();
        $clicker.addClass('expanded');
    }

    function hideSearch($target, $clicker) {
        $clicker.removeClass('expanded');
        $('.search_container').removeClass('searchopen');
        $('.topmenu').removeClass('searchopen');
        $target.slideUp();
    }
	$('.userheader_overview').on('click', function(i) {
		var $parent = $(this).parents('.mindup-userfunctions').toggleClass('opened');
	});
    $('.expander_section').each(function(i) {
        var $sec = $(this);
        if ($sec.data('collapse') == 'no')
            return false;
        var $exp = $sec.find('.expander_contents').fadeOut();
        var $lnk = $sec.find('.expander_header a').on('click', function(e) {
            e.preventDefault();
            if ($sec.is('.opened')) {
                $exp.hide();
                $sec.removeClass('opened');
                $lnk.removeClass('activated');
            } else {
                $exp.fadeIn('fast');
                $sec.addClass('opened');
                $lnk.addClass('activated');
            }
        });
    });
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 1) {
            $("body").addClass("socialink-scrolling");
        } else {
            $("body").removeClass("socialink-scrolling");
        }
    });
    // Flying Focus - http://n12v.com/focus-transition/
    (function() {

        if (document.getElementById('flying-focus')) return;

        var flyingFocus = document.createElement('flying-focus'); // use uniq element name to decrease the chances of a conflict with website styles
        flyingFocus.id = 'flying-focus';
        document.body.appendChild(flyingFocus);

        var DURATION = 100;
        flyingFocus.style.transitionDuration = flyingFocus.style.WebkitTransitionDuration = DURATION / 1000 + 's';

        function offsetOf(elem) {
            var rect = elem.getBoundingClientRect();
            var docElem = document.documentElement;
            var win = document.defaultView;
            var body = document.body;

            var clientTop = docElem.clientTop || body.clientTop || 0,
                clientLeft = docElem.clientLeft || body.clientLeft || 0,
                scrollTop = win.pageYOffset || docElem.scrollTop || body.scrollTop,
                scrollLeft = win.pageXOffset || docElem.scrollLeft || body.scrollLeft,
                top = rect.top + scrollTop - clientTop,
                left = rect.left + scrollLeft - clientLeft;

            return { top: top, left: left };
        }

        var movingId = 0;
        var prevFocused = null;
        var isFirstFocus = true;
        var keyDownTime = 0;

        document.documentElement.addEventListener('keydown', function(event) {
            var code = event.which;
            // Show animation only upon Tab or Arrow keys press.
            if (code === 9 || (code > 36 && code < 41)) {
                keyDownTime = now();
            }
        }, false);

        document.documentElement.addEventListener('focus', function(event) {
            var target = event.target;
            if (target.id === 'flying-focus') {
                return;
            }
            var offset = offsetOf(target);
            flyingFocus.style.left = offset.left + 'px';
            flyingFocus.style.top = offset.top + 'px';
            flyingFocus.style.width = target.offsetWidth + 'px';
            flyingFocus.style.height = target.offsetHeight + 'px';

            if (isFirstFocus) {
                isFirstFocus = false;
                return;
            }

            if (now() - keyDownTime > 42) {
                return;
            }

            onEnd();
            target.classList.add('flying-focus_target');
            flyingFocus.classList.add('flying-focus_visible');
            prevFocused = target;
            movingId = setTimeout(onEnd, DURATION);
        }, true);

        document.documentElement.addEventListener('blur', function() {
            onEnd();
        }, true);


        function onEnd() {
            if (!movingId) {
                return;
            }
            clearTimeout(movingId);
            movingId = 0;
            flyingFocus.classList.remove('flying-focus_visible');
            prevFocused.classList.remove('flying-focus_target');
            prevFocused = null;
        }

        function now() {
            return new Date().valueOf();
        }

        // Properly update the ARIA states on focus (keyboard) and mouse over events
        $('[role="menubar"]').on('focus.aria  mouseenter.aria', '[aria-haspopup="true"]', function(ev) {
            $(ev.currentTarget).attr('aria-expanded', true);
        });

        // Properly update the ARIA states on blur (keyboard) and mouse out events
        $('[role="menubar"]').on('blur.aria  mouseleave.aria', '[aria-haspopup="true"]', function(ev) {
            $(ev.currentTarget).attr('aria-expanded', false);
        });


    })();
    console.log('** END loading Social Ink site');
	
	$('#videoModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var source_url = button.data('url');
		$('#modalVid').html("<source src='"+source_url+"' type='video/mp4' />");
		var video = document.getElementById('modalVid');
		video.src = source_url;
		video.play();
	});
	
	$('#videoModalClose').on('click',function(){
		$("#videoModal").modal('hide');
	});
	
	$('#videoModal').on('hide.bs.modal', function (event) {
		document.getElementById('modalVid').pause();
		document.getElementById('modalVid').currentTime = 0;
	});
	
	//expand license information on open source attributions page
	$('.license_expand').on('click',function(e){
		e.preventDefault();
		$(this).parent().parent().parent().find('.license_text').toggle();
	});

});