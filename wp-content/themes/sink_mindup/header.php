<!DOCTYPE html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js ie6 oldie"><![endif]-->
<!--[if IE 7]>	 <html <?php language_attributes(); ?> class="no-js ie7 oldie"><![endif]-->
<!--[if IE 8]>	 <html <?php language_attributes(); ?> class="no-js ie8 oldie"><![endif]-->
<!--[if IE 9]>	 <html <?php language_attributes(); ?> class="no-js ie9 oldie"><![endif]-->
<!--[if gt IE 9]><!--><html <?php language_attributes(); ?> class=""> <!--<![endif]-->
<head>
	<title><?php bloginfo('name');?><?php wp_title(); ?></title>
	<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
	<meta name="description" content="<?php bloginfo('description') ?>" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<?php if(is_search()) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php }?>
	
	<!-- Favicon : BEGIN -->

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/images/mindup-favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
	<!-- Favicon : END -->
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php wp_enqueue_script('jquery');  ?>
	<?php wp_head(); ?>
		<!--[if IE]>
          /* IE 9 and lower only */
          <style type="text/css">
               .gradient {
                    filter: none;
               }
          </style>
          <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/css/ie.css" media="screen" />
	     		<script>
               document.createElement('header');
               document.createElement('nav');
               document.createElement('section');
               document.createElement('article');
               document.createElement('aside');
               document.createElement('main');
               document.createElement('footer');
	     		</script>
          /* END IE 9 and lower only */
		<![endif]-->
</head>
<?php 	$heroclass = get_hero_class();	?>
<body <?php body_class($heroclass) ?>>
<!-- header.php : BEGIN -->

	<?php get_searchbox(true) ?>
	<div id="container" class="">
	     <div id="skiptocontent"><a href="#maincontent" class="skipping_button">skip to main content</a></div>
	     <div class="container_inner">
			<header id="header" role="banner">
				<div class="centering_box extended_centering nav-768">
				<?php get_header_inner() ?>
				</div>
			</header>
			<?php 	echo get_hero_images();
				$sinkLearnDash = new sinksinkLearnDash();
				$sinkLearnDash->ld_course_info_widget();
			 ?>
<!-- header.php : END -->