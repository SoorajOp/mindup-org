<?php
/*
Template Name: Courses Overview
*/
?>
<?php get_header(); ?>
<!-- page-my-courses-template.php | TEMPLATE: Courses Overview : BEGIN -->
<main class="onepage" id="maincontent" role="main">
	<section class="content">
		<?php if (have_posts()) {
			the_post(); 	?>
			<article <?php post_class('copy') ?>>
				<header class="pageinfo">
					<h1><?php the_title() ?></h1>
				</header>
				<div class="text">
					<?php the_content(); ?>
				</div>
				<?php if(current_user_can('mepr-active','rules:63')): ?>
				<?php the_widget('Course_Widget') ?>
				<?php endif; ?>
			</article>
		<?php } ?>
	</section>
 </main>
<!-- page-my-courses-template.php | TEMPLATE: Courses Overview : END -->
<?php get_footer(); ?>