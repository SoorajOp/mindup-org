<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>
<main class="onepage" id="maincontent" role="main">
<section class="content centering_box">
	<article <?php post_class('copy') ?>>
	</article>
</section>
</main>
<?php get_footer(); ?>