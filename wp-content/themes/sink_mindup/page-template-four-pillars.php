<?php
/*
Template Name: MindUP Program (4 Pillars)
*/
?>
<?php 	get_header();
		global $post;
		$main = $post;
		the_post();
 ?>

 <!-- page-template-four-pillars.php | TEMPLATE: MindUP Program (4 Pillars) : BEGIN -->
 <main class="onepage" id="maincontent" role="main">
	 <article <?php post_class('copy') ?>>
		 

		 <!-- MindUP Program : BEGIN -->
		 	<div class="title-back">
		 		<section class="container-md px-md-0">
			 		<div class="row">
			 			<div class="col-12">
			 				<h1><?php the_title(); ?></h1>
			 			
			 				<?php the_content(); ?>
			 				<?php
					 				/* grab the url for the full size featured image */
        					$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
					 		?>
					 		<img class="img-fluid" src="<?php echo $featured_img_url; ?>" />
			 			</div>
			 		</div>
			 	</section>
		 	</div>
		 	<?php get_pillarimporter(); ?>
		 	<?php	get_cardimporter(); ?>
	 </article>
	 
</main>
<!-- page-template-four-pillars.php | TEMPLATE: MindUP Program (4 Pillars) : END -->
<?php get_footer(); ?>