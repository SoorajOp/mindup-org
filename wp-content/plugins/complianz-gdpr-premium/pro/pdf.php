<?php
# No need for the template engine
define('WP_USE_THEMES', false);

#find the base path
define('BASE_PATH', find_wordpress_base_path() . "/");

# Load WordPress Core
require_once(BASE_PATH . 'wp-load.php');
require_once(BASE_PATH . 'wp-includes/class-phpass.php');
require_once(BASE_PATH . 'wp-admin/includes/image.php');

if (isset($_GET['nonce'])) {
    $nonce = $_GET['nonce'];
    if (!wp_verify_nonce($nonce, 'cmplz_pdf_nonce')) {
        die("invalid command");
    }
} else {
    die("invalid command");
}

if (!isset($_GET['post_id']) && !isset($_GET['page'])) {
    die('invalid command');
}


$region = isset($_GET['region']) ? sanitize_title($_GET['region']) : 'eu';
$save_to_file = isset($_GET['save']) ? true:false;

if (isset($_GET['post_id'])) {
    $post_id = intval($_GET['post_id']);
    $post_type = get_post_type($post_id);
    $type = str_replace('cmplz-', '', $post_type);
} else {
    $page = sanitize_title($_GET['page']);
    $region = cmplz_get_region_from_legacy_type($page);
    $type = str_replace('-'.$region, '', $page);
    $post_id = false;
}

COMPLIANZ::$document->generate_pdf($type, $region, $post_id, $save_to_file);
if (!$save_to_file) exit;

//==============================================================
//==============================================================
//==============================================================

function find_wordpress_base_path()
{
    $dir = dirname(__FILE__);
    do {
        //it is possible to check for other files here
        if (file_exists($dir . "/wp-config.php")) {
            return $dir;
        }
    } while ($dir = realpath("$dir/.."));
    return null;
}
