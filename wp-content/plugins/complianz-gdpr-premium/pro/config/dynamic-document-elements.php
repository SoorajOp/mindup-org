<?php
defined( 'ABSPATH' ) or die( "you do not have acces to this page!" );

add_filter( 'cmplz_document_elements', 'cmplz_dynamic_pro_document_elements', 10, 4 );
function cmplz_dynamic_pro_document_elements( $elements, $region, $type, $fields ) {
	if ( $type === 'privacy-statement' ) {

		$options = get_option( 'complianz_options_wizard' );

		$purposes = isset( $options['purpose_personaldata'] ) ? $options['purpose_personaldata'] : array();

		$purpose_elements = array(
			'purpose' => array(
				'p'         => false,
				'numbering' => true,
				'title'     => __( "Purpose, data and retention period", 'complianz-gdpr' ),
				'content'   => '',
			)
		);

		foreach ( $purposes as $key => $value ) {
			if ( $value != 1 ) {
				continue;
			}

			//a key might not exist if we just disabled US, and had selected an option which is not available in the EU.
			if ( ! isset( $fields['purpose_personaldata']['options'][ $key ] ) ) {
				continue;
			}

			$label            = $fields['purpose_personaldata']['options'][ $key ];
			$purpose_elements = $purpose_elements +
			                    array(
				                    $key . '_title'          => array(
					                    'p'         => false,
					                    'dropdown-open'  => true,
					                    'dropdown-title' => $label,
					                    'dropdown-class' => 'dropdown-privacy-statement',
					                    'condition' => array( 'purpose_personaldata' => $key ),
				                    ),
				                    $key . '_gegevens'       => array(
					                    'p'         => false,
					                    'subtitle'  => __( 'For this purpose we use the following data:', 'complianz-gdpr' ),
					                    'numbering' => false,
					                    'content'   => '[' . $key . '_data_purpose]',
					                    'condition' => array( 'purpose_personaldata' => $key ),
				                    ),
				                    $key . '_gegevens_other' => array(
					                    'p'         => false,
					                    'numbering' => false,
					                    'content'   => '[' . $key . '_specify_data_purpose]',
					                    'condition' => array( $key . '_data_purpose' => 16 ),
					                    'class'     => 'cmplz-indent',
				                    ),

				                    $key . '_processing_data_lawfull' => array(
					                    'p'         => false,
					                    'subtitle'  => __( 'The basis on which we may process these data is:', 'complianz-gdpr' ),
					                    'numbering' => false,
				                    ),
				                    $key . '_lawful-basis-2'          => array(
					                    'p'         => false,
					                    'content'   => '<a target="_blank" rel="noopener noreferrer nofollow" href="https://cookiedatabase.org/legal-bases/#consent">'. __( 'Consent obtained', 'complianz-gdpr' ).'</a>',
					                    'numbering' => false,
					                    'list'      => true,
					                    'condition' => array( $key . '_processing_data_lawfull' => '1' ),
				                    ),
				                    $key . '_lawful-basis-3'          => array(
					                    'p'         => false,
					                    'content'   => '<a target="_blank" rel="noopener noreferrer nofollow" href="https://cookiedatabase.org/legal-bases/#agreement">'. __( 'Performance of an agreement ', 'complianz-gdpr' ) .'</a>',
					                    'numbering' => false,
					                    'list'      => true,
					                    'condition' => array( $key . '_processing_data_lawfull' => '2' ),
				                    ),
				                    $key . '_lawful-basis-4'          => array(
					                    'p'         => false,
					                    'content'   => '<a target="_blank" rel="noopener noreferrer nofollow" href="https://cookiedatabase.org/legal-bases/#obligation">'. __( 'Legal obligation', 'complianz-gdpr' ) .'</a>',
					                    'numbering' => false,
					                    'list'      => true,
					                    'condition' => array( $key . '_processing_data_lawfull' => '3' ),
				                    ),
				                    $key . '_lawful-basis-5'          => array(
					                    'p'         => false,
					                    'content'   => '<a target="_blank" rel="noopener noreferrer nofollow" href="https://cookiedatabase.org/legal-bases/#public">'. __( 'Performance of a public task', 'complianz-gdpr' ) .'</a>',
					                    'numbering' => false,
					                    'list'      => true,
					                    'condition' => array( $key . '_processing_data_lawfull' => '4' ),
				                    ),
				                    $key . '_lawful-basis-6'          => array(
					                    'p'         => false,
					                    'content'   => '<a target="_blank" rel="noopener noreferrer nofollow" href="https://cookiedatabase.org/legal-bases/#legitimate">'. __( 'A legitimate interest of our own', 'complianz-gdpr' ) .'</a>',
					                    'numbering' => false,
					                    'list'      => true,
					                    'condition' => array( $key . '_processing_data_lawfull' => '5' ),
				                    ),

				                    $key . '_retain_data' => array(
					                    'p'         => false,
					                    'subtitle'  => __( 'Retention period', 'complianz-gdpr' ),
					                    'list'      => true,
					                    'numbering' => false,
					                    'condition' => array(
						                    'purpose_personaldata' => $key
					                    ),
				                    ),

				                    $key . '_retain_until_terminated' => array(
					                    'p'         => false,
					                    'numbering' => false,
					                    'list'      => true,
					                    'content'   => __( "We retain this data until the service is terminated.", 'complianz-gdpr' ),
					                    'condition' => array( $key . '_retain_data' => '1' ),
				                    ),

				                    $key . '_retain_until_terminated_nr_months' => array(
					                    'p'         => false,
					                    'numbering' => false,
					                    'list'      => true,
					                    'content'   => sprintf( __( "We retain this data upon termination of the service for the following number of months: %s", 'complianz-gdpr' ), '[' . $key . '_retention_period_months]' ),
					                    'condition' => array( $key . '_retain_data' => '2' ),
				                    ),

				                    $key . '_retain_until_terminated_period' => array(
					                    'p'         => false,
					                    'numbering' => false,
					                    'content'   => sprintf( __( "Upon termination of the service we retain this data for the following period: %s.", 'complianz-gdpr' ), '[' . $key . '_retain_wmy]' ),
					                    'condition' => array( $key . '_retain_data' => '3' ),
				                    ),

				                    $key . '_description_criteria_retention' => array(
					                    'p'         => false,
					                    'numbering' => false,
					                    'content'   => sprintf( __( 'We determine the retention period according to fixed objective criteria: %s', 'complianz-gdpr' ), '[' . $key . '_description_criteria_retention]' ),
					                    'condition' => array( $key . '_retain_data' => '4' ),
				                    ),
				                    $key . '_dropdown_close'          => array(
					                    'p'         => false,
					                    'dropdown-close'  => true,
					                    'condition' => array( 'purpose_personaldata' => $key ),
				                    ),

			                    );
		}
		//EU
		if ( $region === 'eu' ) {

			if ( count( $purpose_elements ) > 1 ) {

				$elements = array_slice( $elements, 0, 2, true ) +
				            $purpose_elements +
				            array_slice( $elements, 2, count( $elements ) - 2, true );

			}
		}


		//UK
		if ( $region === 'uk' ) {
			if ( count( $purpose_elements ) > 1 ) {

				$elements = array_slice( $elements, 0, 2, true ) +
				            $purpose_elements +
				            array_slice( $elements, 2, count( $elements ) - 2, true );

			}
		}
	}



	/*
	 * US: intentionally not translatable
	 *
	 * */


	if ( ($region === 'us' || $region === 'ca') && $type == 'privacy-statement' ) {
		$purpose_elements_us = array(
			'purpose' => array(
				'p'         => false,
				'numbering' => true,
				'title'     => __("Purpose and categories of data", "complianz-gdpr"),
				'content'   => __('Categories of personal information to be collected and the purpose for which the categories shall be used.', "complianz-gdpr"),
			)
		);
		foreach ( $purposes as $key => $value ) {
			if ( $value != 1 ) {
				continue;
			}

			//a key might not exist if we just disabled US, and had selected an option which is not available in the EU.
			if ( ! isset( $fields['purpose_personaldata']['options'][ $key ] ) ) {
				continue;
			}

			$label               = $fields['purpose_personaldata']['options'][ $key ];
			$purpose_elements_us = $purpose_elements_us +
			                       array(
				                       $key . '_us_title'      => array(
					                       'p'         => false,
					                       'subtitle'  => __('We use your data for the following purpose:', "complianz-gdpr"),
					                       'content'   => $label,
					                       'numbering' => true,
					                       'condition' => array( 'purpose_personaldata' => $key )
				                       ),
				                       $key . '_us_categories' => array(
					                       'p'         => false,
					                       'subtitle'  => __('The following categories of data are collected', "complianz-gdpr"),
					                       'content'   => '[' . $key . '_data_purpose_us]',
					                       'numbering' => false,
					                       'condition' => array( 'purpose_personaldata' => $key )
				                       ),
			                       );
		}
		if ( count( $purpose_elements_us ) > 1 ) {

			$elements = array_slice( $elements, 0, 2, true ) +
			            $purpose_elements_us +
			            array_slice( $elements, 2, count( $elements ) - 2, true );

		}
	}

	return $elements;
}
