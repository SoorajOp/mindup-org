<?php
defined('ABSPATH') or die("you do not have acces to this page!");
$this->fields = $this->fields + array(
        'has-wizard-been-completed-ca' => array(
            'step' => 1,
            'source' => 'dataleak-ca',
            'type' => 'radio',
            'default' => '',
            'callback' => 'is_wizard_completed',
        ),

        'security-incident-occurred-ca' => array(
            'step' => 2,
            'section' => 1,
            'source' => 'dataleak-ca',
            'type' => 'radio',
            'required' => true,
            'default' => '',
            'label' => __("Has a security incident occurred?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'tooltip' => __("e.g. lost USB stick, break in, stolen laptop, password lost.", 'complianz-gdpr'),
        ),

        'type-of-dataloss-ca' => array(
            'step' => 2,
            'section' => 2,
            'source' => 'dataleak-ca',
            'type' => 'radio',
            'default' => '',
            'required' => true,
            'label' => __("Which situation applies to the incident above.", 'complianz-gdpr'),
            'callback_condition' => array('security-incident-occurred-ca' => 'yes'),
            'options' => array(
                '1' => __('Personal data has been lost, and there is no up to date back-up', 'complianz-gdpr'),
                '2' => __('It can not be excluded that unauthorized persons have gained access to personal data', 'complianz-gdpr'),
                '3' => __('The above alternatives do not apply.', 'complianz-gdpr'),
            ),
        ),
        'risk-of-data-loss-ca' => array(
            'step' => 3,
            'source' => 'dataleak-ca',
            'type' => 'radio',
            'required' => true,
            'options' => array(
                '1' => __("There is a real risk of significant harm, due to the probability that the personal information has been, is being or will be misused.", 'complianz-gdpr'),
                '2' => __("The databreach applies to (some) personal data that may be sensitive.", 'complianz-gdpr'),
                '3' => __("The data has been encrypted in such a way that it is not possible to abuse the data", 'complianz-gdpr'),
                '4' => __("The possible consequences have been minimized immediately, which effectively excludes the possibility of abuse by malicious parties", 'complianz-gdpr'),
            ),
            'default' => '',
            'label' => __("What information was involved?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
            ),
        ),
        'can-reduce-risk' => array(
            'step' => 4,
            'source' => 'dataleak-ca',
            'type' => 'radio',
            'required' => true,
            'options' =>$this->yes_no,
            'default' => '',
            'label' => __("Do you think any other organization, a government institution or a part of a government institution may be able to reduce the risk of harm from the breach or to mitigate that harm?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),
            ),
        ),


        'what-occurred-ca' => array(
            'step' => 4,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What has occurred exactly?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),
            ),
        ),
        'consequences-ca' => array(
            'step' => 4,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'required' => true,
            'default' => '',
            'label' => __("What are the possible consequences?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),

            ),
        ),
        'measures-ca' => array(
            'step' => 4,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What measures have been taken after the breach?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),

            ),
        ),

        'measures_by_person_involved-ca' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What measures could a person involved take to minimize damage?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),

            ),
        ),

        'date-of-breach-ca' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("Day on which, or period during which the breach occurred, or if neither is known, the approximate period.", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),

            ),
        ),



        'phone-url-inquiries-ca' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-ca',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("Through which email address can customers make inquiries about your system?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-ca' => 'NOT 3',
                'security-incident-occurred-ca' => 'yes',
                'risk-of-data-loss-ca' => array('NOT 3', 'NOT 4'),

            ),
        ),

        'conclusion-ca' => array(
            'step' => 5,
            'source' => 'dataleak-ca',
            'callback' => 'dataleak_conclusion',
            'type' => 'text',
            'default' => '',
        ),
    );
