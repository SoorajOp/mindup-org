<?php
defined('ABSPATH') or die("you do not have acces to this page!");

$this->pages['uk']['privacy-statement']['document_elements'] = array(
    'last-updated' => array(
        'content' => '<i>' . sprintf('This privacy statement was last updated on %s and applies to citizens of the United Kingdom.', '[publish_date]') . '</i>',
    ),
    'inleiding' => array(
        'p' => false,
        'content' =>
            '<p>'.sprintf('In this privacy statement, we explain what we do with the data we obtain about you via %s. We recommend you carefully read this statement. In our processing we comply with the requirements of privacy legislation. That means, among other things, that:', '[domain]') .'</p>'.
            '<ul>
                <li>' . 'we clearly state the purposes for which we process personal data. We do this by means of this privacy statement;' . '</li>
                <li>' . 'we aim to limit our collection of personal data to only the personal data required for legitimate purposes;' . '</li>
                <li>' . 'we first request your explicit consent to process your personal data in cases requiring your consent;' . '</li>
                <li>' . 'we take appropriate security measures to protect your personal data and also require this from parties that process personal data on our behalf;' . '</li>
                <li>' . 'we respect your right to access your personal data or have it corrected or deleted, at your request.' . '</li>
            </ul>' .
            '<p>'.'If you have any questions, or want to know exactly what data we keep of you, please contact us.'.'</p>',
    ),

    //In the privacy-policy page the first paragraph containing purpose and data retention period is generated in the class-config.php
    'third-party-sharing' => array(
        'title' => 'Sharing with other parties',
        'content' => 'We only share this data with processors and with other third parties for which the data subjects consent must be obtained. It concerns the following party or parties:',
        'condition' => array('share_data_other' => '1'),
    ),

    //this has to be above the processors, as processors are shown for both 1 and 3. It's condition is then "Not 2"
    'no-sharing-limited' => array(
        'p' => true,
        'title' => 'Sharing with other parties',
        'content' => 'We only share or disclose this data to processors for the following purposes:',
        'condition' => array('share_data_other' => '3'),
    ),

    'no-sharing-limited-sub' => array(
        'p' => true,
        'numbering' => false,
        'subtitle' => 'Processors',
        'condition' => array('share_data_other' => 'NOT 2'),
    ),

    'processor' => array(
        'p' => false,
        'numbering' => false,
        'content' =>
            "<p>
                <b>" . "Name:" . "</b>&nbsp;[name]<br>
                <b>" . "Country:" . "</b>&nbsp;[country]<br>
                <b>" . "Purpose:" . "</b>&nbsp;[purpose]<br>
            </p>",
        'condition' => array(
            'processor' => 'loop',
            'share_data_other' => 'NOT 2',
        ),
    ),
    'no-sharing-limited-sub2' => array(
        'p' => true,
        'numbering' => false,
        'subtitle' => 'Third parties',
        'condition' => array('share_data_other' => '1'),
    ),

    'thirdparty' => array(
        'p' => false,
        'numbering' => false,
        'content' =>
            "<p>
                <b>" . "Name:" . "</b>&nbsp;[name]<br>
                <b>" . "Country:" . "</b>&nbsp;[country]<br>
                <b>" . "Purpose:" . "</b>&nbsp;[purpose]<br>
                <b>" . "Data:" . "</b>&nbsp;[data]
            </p>",
        'condition' => array(
            'thirdparty' => 'loop',
            'share_data_other' => '1',
        ),
    ),

    'no-sharing' => array(
        'p' => false,
        'title' => 'Sharing with other parties',
        'content' => 'We do not share your data with third parties.',
        'condition' => array('share_data_other' => '2'),
        'callback_condition' => 'NOT cmplz_site_shares_data', //even though someone may have entered he doesn't share data, it may be possible data is shared with vendors.
    ),

    'privacy-policy-cookies' => array(
        'p' => false,
        'title' => 'Cookies',
        'content' => sprintf('Our website uses cookies. For more information about cookies, please refer to our %sCookie Policy%s.', '<a href="[cookie-statement-url]">', '</a>')."&nbsp;",
        'condition' => array(
	        'uses_ad_cookies_personalized' => 'NOT tcf',
        ),
    ),

    array(
	    'p' => false,
	    'title' => _x('Cookies', 'Legal document privacy statement:paragraph title', 'complianz-gdpr'),
	    'content' => sprintf(_x('To provide the best experiences, we and our partners use technologies like cookies to store and/or access device information. Consenting to these technologies will allow us and our partners to process personal data such as browsing behaviour or unique IDs on this site. Not consenting or withdrawing consent, may adversely affect certain features and functions. For more information about these technologies and partners, please refer to our %sCookie Policy%s.', 'Legal document privacy statement', 'complianz-gdpr'), '<a href="[cookie-statement-url]">', '</a>')."&nbsp;",
	    'condition' => array(
		    'uses_ad_cookies_personalized' => 'tcf',
	    ),
    ),

	array(
		'p' => true,
		'content' => sprintf(_x('%s participates in the IAB Europe Transparency & Consent Framework and complies with its Specifications and Policies. It uses the Consent Management Platform with the identification number %s.', 'Legal document privacy statement', 'complianz-gdpr'), '[organisation_name]', '332')."&nbsp;",
		'condition' => array(
			'uses_ad_cookies_personalized' => 'tcf',
		),
	),

    'statistics_anonymous' => array(
        'p' => false,
        'title' => 'Statistics',
        'content' => 'We keep track of anonymised statistics to gain insight into how often and in what way visitors use our website.',
        'condition' => array('compile_statistics' => 'yes-anonymous'),
    ),

    'statistics-google' => array(
        'p' => false,
        'content' => 'We have concluded a data processing agreement with Google.',
        'callback_condition' => 'cmplz_accepted_processing_agreement',
    ),

    'statistics-no-sharing' => array(
        'p' => false,
        'content' => 'Google may not use the data for any other Google services.',
        'callback_condition' => 'cmplz_statistics_no_sharing_allowed',
    ),

    'statistics-no-ip' => array(
        'p' => false,
        'content' => 'The inclusion of full IP addresses is blocked by us.',
        'callback_condition' => 'cmplz_no_ip_addresses',
    ),

    'security' => array(
        'p' => false,
        'title' => 'Security',
        'content' => 'We are committed to the security of personal data. We take appropriate security measures to limit abuse of and unauthorised access to personal data. This ensures that only the necessary persons have access to your data, that access to the data is protected, and that our security measures are regularly reviewed.'
    ),
    'security_which' => array(
        'p' => true,
        'content' => 'The security measures we use consist of:',
        'condition' => array('secure_personal_data' => 2),
    ),
    'security_which_content' => array(
        'p' => false,
        'content' => '[which_personal_data_secure]',
        'condition' => array('secure_personal_data' => 2),
    ),
    'third-party-website' => array(
        'p' => false,
        'title' => 'Third-party websites',
        'content' => 'This privacy statement does not apply to third-party websites connected by links on our website. We cannot guarantee that these third parties handle your personal data in a reliable or secure manner. We recommend you read the privacy statements of these websites prior to making use of these websites.',
    ),
    'changes-privacy-statement' => array(
        'p' => false,
        'title' => 'Amendments to this privacy statement',
        'content' => 'We reserve the right to make amendments to this privacy statement. It is recommended that you consult this privacy statement regularly in order to be aware of any changes. In addition, we will actively inform you wherever possible.',
    ),
    'insight-changes-your-data' => array(
        'title' => 'Accessing and modifying your data',
        'p'=>false,
        'content' =>
            '<p>'.'If you have any questions or want to know which personal data we have about you, please contact us. You can contact us by using the information below. You have the following rights:' .'</p>'.
            '<ul>
                <li>' . 'You have the right to know why your personal data is needed, what will happen to it, and how long it will be retained for.' . '</li>
                <li>' . 'Right of access: You have the right to access your personal data that is known to us.' . '</li>
                <li>' . 'Right to rectification: you have the right to supplement, correct, have deleted or blocked your personal data whenever you wish.' . '</li>
                <li>' . 'If you give us your consent to process your data, you have the right to revoke that consent and to have your personal data deleted.' . '</li>
                <li>' . 'Right to transfer your data: you have the right to request all your personal data from the controller and transfer it in its entirety to another controller.' . '</li>
                <li>' . 'Right to object: you may object to the processing of your data. We comply with this, unless there are justified grounds for processing.' . '</li>
            </ul>' .
            '<p>'.'Please make sure to always clearly state who you are, so that we can be certain that we do not modify or delete any data of the wrong person.'.'</p>',
    ),

    'automated_processes' => array(
        'p' => true,
        'title' => 'Automated decision-making',
        'content' => 'We make decisions on the basis of automated processing with respect to matters that may have (significant) consequences for individuals. These are decisions taken by computer programmes or systems without human intervention.',
        'condition' => array('automated_processes' => 'yes'),
    ),

    'automated_processes_details' => array(
        'p' => true,
        'content' => '[automated_processes_details]',
        'condition' => array('automated_processes' => 'yes'),
    ),

    'complaints' => array(
        'p' => false,
        'title' => 'Submitting a complaint',
        'content' => "If you are not satisfied with the way in which we handle (a complaint about) the processing of your personal data, you have the right to submit a complaint to the Information Commissioner's Office:",
    ),

    'complaints-address' => array(
        'p' => false,
        'content' => 'Wycliffe House<br>
                        Water Lane<br>
                        Wilmslow<br>
                        Cheshire<br>
                        SK9 5AF',
    ),

    'data-protection-officer' => array(
        'p' => false,
        'title' => 'Data Protection Officer',
        'content' => sprintf("Our Data Protection Officer has been registered with the Information Commissioner's Office. If you have any questions or requests with respect to this privacy statement or for the Data Protection Officer, you may contact %s, %s or via %s.", '[name_uk_dpo]', '[website_uk_dpo]', '[email_uk_dpo]'),
        'condition' => array('dpo_or_uk_gdpr' => 'dpo'),
    ),

    array(
            'title' => 'Children',
            'content' => 'Our website is not designed to attract children and it is not our intent to collect personal data from children under the age of consent in their country of residence. We therefore request that children under the age of consent do not submit any personal data to us.',
            'condition' => array('targets-children' => 'no'),
        ),

    array(
        'title' => 'Children',
        'content' => sprintf("For our privacy statement regarding children, please see our dedicated %sChildren's Privacy Statement%s", '<a href="[privacy-statement-children-uk-url]">', '</a>'),
        'condition' => array('targets-children' => 'yes'),
    ),

    'contact-details' => array(
        'title' => 'Contact details',
        'content' => '[organisation_name]<br>
        [address_company]<br>
        [country_company]<br>
        ' . 'Website:' . ' [domain] <br>
        ' . 'Email:' . ' [email_company] <br>
        [telephone_company]',
    ),
    'uk_gdpr_rep' => array(
        'p' => false,
        'content' => sprintf('We have appointed a representative within the United Kingdom. If you have any questions or requests with respect to this privacy statement or for our representative, you may contact %s, via %s, %s or by telephone on %s.', '[name_uk_gdpr]', '[email_uk_gdpr]', '[website_uk_gdpr]', '[phone_uk_gdpr]'),
        'condition' => array('dpo_or_uk_gdpr' => 'uk_gdpr_rep'),
    ),
    /* Dit zijn de privacy policies die door wp worden aangeboden per plugin */
    'custom_privacy_policy_text' => array(
        'p' => false,
        'title' => 'Annex',
        'numbering' => false,
        'content' => '[custom_privacy_policy_text]',
        'callback_condition' => 'cmplz_has_custom_privacy_policy',
    ),

// End privacy statement array
);
