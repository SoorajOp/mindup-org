<?php
/*
 * This document is intentionally not translatable, as it is intended to be for DE/AU citizens, and should therefore always be in German
 *
 * */
defined('ABSPATH') or die("you do not have acces to this page!");

$this->pages['eu']['impressum']['document_elements'] = array(
	array(
		'content' => '<i>' . sprintf('Dieses Impressum wurde zuletzt am %s aktualisiert', '[publish_date]') . '</i>',
	),

	array(
		'title' => 'Name',
		'numbering' => false,
		'content' => '[organisation_name]',
	),

	array(
		'title' => 'Adresse',
		'numbering' => false,
		'content' => '[address_company]<br>
                      Email: [email_company]<br>
					  Telefonnummer: [telephone_company]',
	),

	array(
		'title' => 'Verantwortlich für den Inhalt nach § 18 Abs. 2 MStV',
		'numbering' => false,
		'content' => '[editorial_responsible]',
		'condition' => array('offers_editorial_content' => 'yes'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Umsatzsteuer-ID:</b> [vat_company]',
		'condition' => array('vat_company' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Geltungsbereich:</b> [site_url]',
	),

	array(
		'numbering' => false,
		'content' => '<b>Aufsichtsbehörde</b>: [inspecting_authority]',
		'condition' => array('inspecting_authority' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Register</b>: [register]',
		'condition' => array('register' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Register-Nummer</b>: [business_id]',
		'condition' => array('business_id' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Vertretungsberechtige(r)</b>: [representative]',
		'condition' => array('representative' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Kapitaleinlagen</b>: [capital_stock]',
		'condition' => array('capital_stock' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Berufsgenossenschaft</b>: [professional_association]',
		'condition' => array('professional_association' => 'NOT EMPTY'),
	),

	array(
		'content' => '<b>Gesetzliche Berufsbezeichnung</b>: [legal_job_title]',
		'condition' => array('legal_job_title' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'content' => '<b>Berufsrechtliche Regelungen</b>: [professional_regulations]',
		'condition' => array('professional_regulations' => 'NOT EMPTY'),
	),

	array(
		'numbering' => false,
		'title' => 'Online-Streitbeilegung',
		'content' => 'Die EU-Kommission stellt eine benutzerfreundliche Plattform zur Online-Beilegung von verbraucherrechtlichen Streitigkeiten, die sich aus dem online Verkauf von Waren oder der online Erbringung von Dienstleistungen ergeben (OS-Plattform), bereit. Die OS-Plattform ist unter folgendem Link erreichbar: <a target="_blank" href="https://ec.europa.eu/consumers/odr">https://ec.europa.eu/consumers/odr</a>',
		'condition' => array(
			'is_webshop' => true
		),
	),

	array(
		'numbering' => false,
		'content' => 'Die Firma [organisation_name] ist weder bereit noch verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.',
		'condition' => array(
				'is_webshop' => true,
				'has_webshop_obligation' => 'no'
		),
	),
	array(
		'numbering' => false,
		'content' => 'Die Firma [organisation_name] ist bereit, an Streitbeilegungsverfahren bei einer Verbraucherschlichtungsstelle teilzunehmen',
		'condition' => array(
			'is_webshop' => true,
			'has_webshop_obligation' => 'yes'
		),
	),


);
