<?php
defined('ABSPATH') or die("you do not have acces to this page!");
$this->fields = $this->fields + array(
        'has-wizard-been-completed-us' => array(
            'step' => 1,
            'source' => 'dataleak-us',
            'type' => 'radio',
            'default' => '',
            'callback' => 'is_wizard_completed',
        ),

        'security-incident-occurred-us' => array(
            'step' => 2,
            'section' => 1,
            'source' => 'dataleak-us',
            'type' => 'radio',
            'required' => true,
            'default' => '',
            'label' => __("Has a security incident occurred?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'tooltip' => __("e.g. lost USB stick, break in, stolen laptop, password lost.", 'complianz-gdpr'),
        ),

        'type-of-dataloss-us' => array(
            'step' => 2,
            'section' => 2,
            'source' => 'dataleak-us',
            'type' => 'radio',
            'default' => '',
            'required' => true,
            'label' => __("Which situation applies to the incident above.", 'complianz-gdpr'),
            'callback_condition' => array('security-incident-occurred-us' => 'yes'),
            'options' => array(
                '1' => __('Encrypted personal data is lost, and it cannot be excluded that unauthorized persons have access to the encryption key or password.', 'complianz-gdpr'),
                '2' => __('It can not be excluded that unauthorized persons have gained access to unencrypted personal data', 'complianz-gdpr'),
                '3' => __('The above alternatives do not apply.', 'complianz-gdpr'),
            ),
        ),
        'what-information-was-involved-us' => array(
            'step' => 3,
            'source' => 'dataleak-us',
            'type' => 'radio',
            'required' => true,
            'options' => array(
                'name' => __("An individual’s first name or first initial and last name in combination with any one or more of the data elements (as shown in the next question after selecting this option), when either the name or the data elements are not encrypted", 'complianz-gdpr'),
                'username-email' => __("A user name or email address, in combination with a password or security question and answer that would permit access to an online account.", 'complianz-gdpr'),
                'none' => __("None of the above", 'complianz-gdpr'),
            ),
            'default' => '',
            'label' => __("What information was involved?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
            ),
        ),

        'name-what-us' => array(
            'step' => 3,
            'source' => 'dataleak-us',
            'type' => 'multicheckbox',
            'required' => true,
            'options' => array(
                'social-security-number' => __("Social security number.", 'complianz-gdpr'),
                'drivers-license' => __("Driver’s license number or identification card number.", 'complianz-gdpr'),
                'account-number' => __("Account number or credit or debit card number, in combination with any required security code, access code, or password that would permit access to an individual’s financial account.", 'complianz-gdpr'),
                'medical-info' => __("Medical information.", 'complianz-gdpr'),
                'health-insurance' => __("Health insurance information.", 'complianz-gdpr'),
                'data-collected' => __("Information or data collected through the use or operation of an automated license plate recognition system", 'complianz-gdpr'),
            ),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
            ),
            'condition' => array('what-information-was-involved-us' => 'name'),
            'default' => '',
            'label' => __("Data elements involved in the security breach:", 'complianz-gdpr'),
        ),

//        'what-information-was-involved-us' => array(
//            'step' => 3,
//            'source' => 'dataleak-us',
//            'type' => 'radio',
//            'required' => true,
//            'options' => $this->yes_no,
//            'default' => '',
//            'label' => __("Did the information involved include a user name or email address, in combination with a password or security question and answer that would permit access to an online account?", 'complianz-gdpr'),
//            'callback_condition' => array(
//                'type-of-dataloss-us' => 'NOT 3',
//                'security-incident-occurred-us' => 'yes',
//            ),
//        ),
//
//
//
//        'name-what-us' => array(
//            'step' => 3,
//            'source' => 'dataleak-us',
//            'type' => 'multicheckbox',
//            'required' => false,
//            'options' => array(
//                'social-security-number' => __("Social security number.", 'complianz-gdpr'),
//                'drivers-license' => __("Driver’s license number or identification card number.", 'complianz-gdpr'),
//                'account-number' => __("Account number or credit or debit card number, in combination with any required security code, access code, or password that would permit access to an individual’s financial account.", 'complianz-gdpr'),
//                'medical-info' => __("Medical information.", 'complianz-gdpr'),
//                'health-insurance' => __("Health insurance information.", 'complianz-gdpr'),
//                'data-collected' => __("Information or data collected through the use or operation of an automated license plate recognition system", 'complianz-gdpr'),
//            ),
//            'callback_condition' => array(
//                'type-of-dataloss-us' => 'NOT 3',
//                'security-incident-occurred-us' => 'yes',
//            ),
//            'condition' => array('what-information-was-involved-us' => 'NOT username-email'),
//            'default' => '',
//            'label' => __("Did the information involved include an individual’s first name or first initial and last name in combination with any one or more of the following data elements, when either the name or the data elements are not encrypted?", 'complianz-gdpr'),
//        ),

        'toll-free-phone' => array(
            'step' => 3,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("Please enter the toll-free telephone number and addresses of the major credit reporting agencies:", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
            ),
            'condition' => array(
                'name-what-us' => 'social-security-number OR drivers-license',
            ),
        ),

        'reach-of-dataloss-large-us' => array(
            'step' => 4,
            'section' => 4,
            'source' =>  'dataleak-us',
            'type' => 'radio',
            'default' => '',
            'required' => true,
            'label' => __("Does the security breach affect a large number (500 or more) of people?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
            'options' => $this->yes_no,
        ),

        'california-visitors' => array(
            'step' => 4,
            'section' => 4,
            'source' =>  'dataleak-us',
            'type' => 'radio',
            'default' => '',
            'required' => true,
            'label' => __("Does the databreach affect California residents?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
            'condition' => array('reach-of-dataloss-large-us' => 'yes'),
            'options' => $this->yes_no,
        ),

        'what-occurred-us' => array(
            'step' => 4,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What has occurred exactly?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',

            ),
        ),
        'consequences-us' => array(
            'step' => 4,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'required' => true,
            'default' => '',
            'label' => __("What are the possible consequences?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
        ),
        'measures-us' => array(
            'step' => 4,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What measures have been taken after the breach?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',

            ),
        ),

        'measures_by_person_involved-us' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What measures could a person involved take to minimize damage?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',

            ),
        ),

        'date-of-breach-us' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("What is the date, the approximate date, or the date range within which the security breach has occurred?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
        ),

        'investigation' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("Was the notification delayed as a result of a law enforcement investigation?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
            'options' => $this->yes_no,
        ),

        'phone-url-inquiries-us' => array(
            'step' => 4,
            'section' => 1,
            'source' => 'dataleak-us',
            'type' => 'text',
            'translatable' => false,
            'default' => '',
            'required' => true,
            'label' => __("Through which phone number, or which URL, can customers make inquiries about this security breach?", 'complianz-gdpr'),
            'callback_condition' => array(
                'type-of-dataloss-us' => 'NOT 3',
                'reach-of-dataloss-us' => 'NOT 3',
                'security-incident-occurred-us' => 'yes',
                'what-information-was-involved-us' => 'NOT none',
            ),
        ),

        'conclusion-us' => array(
            'step' => 5,
            'source' => 'dataleak-us',
            'callback' => 'dataleak_conclusion',
            'type' => 'text',
            'default' => '',
        ),
    );
