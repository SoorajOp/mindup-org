<?php
defined('ABSPATH') or die("you do not have acces to this page!");

/*
 * condition: if a question should be dynamically shown or hidden, depending on another answer. Use NOT answer to hide if not answer.
 * callback_condition: if should be shown or hidden based on an answer in another screen.
 * callback roept action cmplz_$page_$callback aan
 * required: verplicht veld.
 * help: helptext die achter het veld getoond wordt.

                "fieldname" => '',
                "type" => 'text',
                "required" => false,
                'default' => '',
                'label' => '',
                'table' => false,
                'callback_condition' => false,
                'condition' => false,
                'callback' => false,
                'placeholder' => '',
                'optional' => false,

* */

// MY COMPANY SECTION
$this->fields = $this->fields + array(

        'free_phonenr' => array(
            'step' => STEP_COMPANY,
            'section' => 3,
            'source' => 'wizard',
            'type' => 'phone',
            'default' => '',
            'required' => false,
            'label' => __("Enter a toll free phone number for the submission of information requests", 'complianz-gdpr'),
            'document_label' => 'Toll free phone number: ',
            'callback_condition' => array(
                'regions' => array('us'),
            ),
            'help' => __('For US based companies, you can provide a toll free phone number for inquiries.','complianz-gdpr').cmplz_read_more('https://complianz.io/toll-free-number/'),
        ),

        'automated_processes' => array(
            'step' => STEP_COMPANY,
            'section' => 3,
            'source' => 'wizard',
            'type' => 'radio',
            'options' => $this->yes_no,
            'required' => true,
            'tooltip' => __("Think of automated processes. For example making predictions on payment behavior based on ZIP code, which influences available paying methods.", 'complianz-gdpr'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('eu','uk'),
            ),
            'label' => __("Do you make decisions based on automated processes, such as profiling, that could have significant consequences for users?", 'complianz-gdpr'),
        ),

        'automated_processes_details' => array(
            'step' => STEP_COMPANY,
            'section' => 3,
            'source' => 'wizard',
            'type' => 'textarea',
            'required' => true,
            'condition'=>array(
                'automated_processes'=>'yes',
            ),
            'label' => __("Specify what kind of decisions these are, what the consequences are, and what (in general terms) the logic behind these decisions is.", 'complianz-gdpr'),
        ),

        'vat_company'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "VAT ID of your company", 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'offers_editorial_content'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'radio',
	        'options'  => $this->yes_no,
	        'label'    => __( "Do you offer content for journalistic and editorial purposes?" , 'complianz-gdpr' ),
	        'help'    => __( "For example websites that run a blog, publish news articles or moderate an online community." , 'complianz-gdpr' ),
	        'required' => true,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'editorial_responsible'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "State the name and place of residence of the person responsible for the content on this website." , 'complianz-gdpr' ),
	        'tooltip'    => __( "The person should be stated with name, last name and place of residence." , 'complianz-gdpr' ),
	        'required' => false,
	        'condition' => array(
		        'offers_editorial_content' => 'yes',
	        ),
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'register'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "In which register of companies, associations, partnerships or cooperatives is your company registered?" , 'complianz-gdpr' ),
	        'tooltip'    => __( "Generally the Chamber of Commerce or a local Court register, but other registers may apply. Leave blank if this does not apply to you." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes'
	        ),
        ),

        'business_id'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Registration number" , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'representative'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Name one or more person(s) who can legally represent the company or legal entity." , 'complianz-gdpr' ),
	        'tooltip'    => __( "This is generally an owner or director of the legal entity." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'capital_stock'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Capital Stock" , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'inspecting_authority'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "If the service or product displayed on this website requires some sort of official approval, state the (inspecting) authority." , 'complianz-gdpr' ),
	        'tooltip'    => __( "For example, a website from a financial advisor might need permission from an inspecting authority." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'professional_association'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Does your website display services or products that require registration with a professional association? If so, name the professional association." , 'complianz-gdpr' ),
	        'tooltip'    => __( "Registration heavily depends on specific national laws. In most countries this obligation applies to Doctors, Pharmacists, Architects, Consulting engineers, Notaries, Patent attorneys, Psychotherapists, Lawyers, Tax consultants, Veterinary surgeons, Auditors or Dentists." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'legal_job_title'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Legal job title. Required if your profession or the activities displayed on the website require a certain diploma." , 'complianz-gdpr' ),
	        'tooltip'    => __( "Required for an activity under a professional title, in so far as the use of such a title is reserved to the holders of a diploma governed by laws, regulations or administrative provisions." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'professional_regulations'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'text',
	        'label'    => __( "Professional Regulations." , 'complianz-gdpr' ),
	        'tooltip'    => __( "If applicable, mention the professional regulations that may apply to your activities, and the URL where to find them." , 'complianz-gdpr' ),
	        'required' => false,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'is_webshop'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'checkbox',
	        'label'    => __( "Do you sell products or services through your website?" , 'complianz-gdpr' ),
	        'tooltip'    => __( "If this is a webshop, the Impressum should include a paragraph about dispute settlement." , 'complianz-gdpr' ),
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
        ),

        'has_webshop_obligation'   => array(
	        'step'     => STEP_COMPANY,
	        'section'  => 4,
	        'source'   => 'wizard',
	        'type'     => 'radio',
	        'options'     => $this->yes_no,
	        'label'    => __( "Are you obliged or prepared to use Alternative Dispute Resolution?" , 'complianz-gdpr' ),
          'tooltip'    => __( "Alternate Dispute Resolution means settling disputes without lawsuit." , 'complianz-gdpr' ),
	        'required' => true,
	        'callback_condition' => array(
		        'impressum' => 'generated',
		        'eu_consent_regions' => 'yes',
	        ),
	        'condition' => array(
	        	'is_webshop' => true
	        ),
        ),

        'ca_name_address_accountable_person' => array(
	        'step' => STEP_COMPANY,
	        'section' => 5,
	        'source' => 'wizard',
	        'type' => 'textarea',
	        'required' => true,
	        'default' => '',
	        'callback_condition' => array(
		        'privacy-statement' => 'generated',
		        'regions' => 'ca',
	        ),
	        'label' => __("What is the name or title, and the address, of the person who is accountable for the organization’s policies and practices and to whom complaints or inquiries can be forwarded?", 'complianz-gdpr'),
        ),

        // DATA PROTECTION OFFICER
        'dpo_or_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'label' => __("Does the following apply to you?", 'complianz-gdpr'),
            'options' => array(
                'dpo' => __('We have appointed a Data Protection Officer', 'complianz-gdpr'),
                'gdpr_rep' => __('We have a GDPR representative within the EU', 'complianz-gdpr'),
                'none' => __('None of the above', 'complianz-gdpr')
            ),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
            'required' => true,
        ),

        'name_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'required' => true,
            'default' => '',
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
            'label' => __("What is the name of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'dpo'),
        ),

        'email_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'email',
            'default' => '',
            'required' => true,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
            'label' => __("What is the email address of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'dpo'),
        ),
        'phone_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'phone',
            'default' => '',
            'required' => false,
            'label' => __("What is the phone number of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'dpo'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'website_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => false,
            'label' => __("If available, enter the website URL of the Data Protection Officer:", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'dpo'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'name_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => true,
            'label' => __("What is the name of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'email_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'email',
            'default' => '',
            'required' => true,
            'label' => __("What is the email address of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'phone_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'phone',
            'default' => '',
            'required' => false,
            'label' => __("What is the phone number of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'website_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => false,
            'label' => __("If available, enter the website URL of the representative:", 'complianz-gdpr'),
            'condition' => array('dpo_or_gdpr' => 'gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'eu',
            ),
        ),

        'dpo_or_uk_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'label' => __("Does the following apply to you?", 'complianz-gdpr'),
            'options' => array(
                'dpo' => __('We have appointed a Data Protection Officer', 'complianz-gdpr'),
                'uk_gdpr_rep' => __('We have a UK-GDPR representative within the United Kingdom', 'complianz-gdpr'),
                'none' => __('None of the above', 'complianz-gdpr')
            ),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
            'required' => true,
        ),

        'name_uk_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'required' => true,
            'default' => '',
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
            'label' => __("What is the name of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'dpo'),
        ),
        'email_uk_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'email',
            'default' => '',
            'required' => true,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
            'label' => __("What is the email address of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'dpo'),
        ),
        'phone_uk_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'phone',
            'default' => '',
            'required' => false,
            'label' => __("What is the phone number of the Data Protection Officer?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'dpo'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),

        'website_uk_dpo' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => false,
            'label' => __("If available, enter the website URL of the Data Protection Officer:", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'dpo'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),

        'name_uk_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => true,
            'label' => __("What is the name of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'uk_gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),

        'email_uk_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'email',
            'default' => '',
            'required' => true,
            'label' => __("What is the email address of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'uk_gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),

        'phone_uk_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'phone',
            'default' => '',
            'required' => false,
            'label' => __("What is the phone number of the representative?", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'uk_gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),

        'website_uk_gdpr' => array(
            'step' => STEP_COMPANY,
            'section' => 5,
            'source' => 'wizard',
            'type' => 'text',
            'default' => '',
            'required' => false,
            'label' => __("If available, enter the website URL of the representative:", 'complianz-gdpr'),
            'condition' => array('dpo_or_uk_gdpr' => 'uk_gdpr_rep'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'uk',
            ),
        ),


        //dynamic purposes here


        // THIRD PARTIES
        'share_data_other' => array(
            'step' => STEP_COMPANY,
            'section' => 9,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('eu','uk'),
            ),
            'options' => array(
                '1' => __('Yes, both to Processors and other Third Parties, whereby the data subject must give permission', 'complianz-gdpr'),
                '2' => __('No', 'complianz-gdpr'),
                '3' => __('Limited: only with Processors that are necessary for the fulfillment of my service', 'complianz-gdpr'),
            ),
            'label' => __("Do you share personal data with other parties?", 'complianz-gdpr'),
            'required' => true,
            'tooltip' => __("Within the GDPR a ‘Processor’ means a natural or legal person, public authority, agency or other body which processes personal data on behalf of the Controller.", 'complianz-gdpr')." ".__("A Third Party is every other entity which receives personal data, but does not fall within the definition of a Processor", 'complianz-gdpr'),

        ),

        'processor' => array(
            'step' => STEP_COMPANY,
            'section' => 9,
            'source' => 'wizard',
            'region' => 'eu',
            'type' => 'processors',
            'required' => false,
            'default' => '',
            'condition' => array('share_data_other' => 'NOT 2'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('eu','uk'),
            ),
            'label' => __("Processors", 'complianz-gdpr'),
            'help' => __('If you share data with processors, add a "processor" for each one, and add the details of the data you share.', 'complianz-gdpr'),
        ),

        'thirdparty' => array(
            'step' => STEP_COMPANY,
            'section' => 9,
            'source' => 'wizard',
            'type' => 'thirdparties',
            'required' => false,
            'default' => '',
            'condition' => array('share_data_other' => '1'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('eu','uk'),
            ),
            'label' => __("Third Parties", 'complianz-gdpr'),
            'help' => __('If you share data with Third Parties, add a "Third Party" for each one, and add the details of the data you share.', 'complianz-gdpr'),
        ),

        'share_data_other_us' => array(
            'step' => STEP_COMPANY,
            'section' => 10,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','ca'),
            ),
            'options' => array(
                '1' => __('Yes, both to Service Providers and other Third Parties', 'complianz-gdpr'),
                '2' => __('No', 'complianz-gdpr'),
                '3' => __('Limited: only with Service Providers that are necessary for the fulfillment of my service', 'complianz-gdpr'),
            ),
            'label' => __("Do you share personal data with other parties?", 'complianz-gdpr'),
            'required' => true,
            'tooltip' => __("A Service Provider is a legal entity that processes information on behalf of a business and to which the business discloses a consumer's personal information for a business purpose pursuant to a written contract.",'complianz-gdpr')." ".__("A Third-party is every other entity which receives personal data, but does not fall within the definition of a Service Provider", 'complianz-gdpr'),
        ),

        'processor_us' => array(
            'step' => STEP_COMPANY,
            'region' => 'us',
            'section' => 10,
            'source' => 'wizard',
            'type' => 'processors',
            'required' => false,
            'default' => '',
            'condition' => array('share_data_other_us' => 'NOT 2'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','ca'),
            ),
            'label' => __("Service Providers", 'complianz-gdpr'),
            'help' => __('If you share data with service providers, add a "Service Provider" for each one, and add the details of the data you share.', 'complianz-gdpr'),
        ),

        'thirdparty_us' => array(
            'step' => STEP_COMPANY,
            'section' => 10,
            'source' => 'wizard',
            'type' => 'thirdparties',
            'required' => false,
            'default' => '',
            'condition' => array('share_data_other_us' => '1'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','ca'),
            ),
            'label' => __("Third-party details", 'complianz-gdpr'),
            'help' => __('If you share data with Third Parties, add a "Third-Party" for each one, and add the details of the data you share.', 'complianz-gdpr'),
        ),

        'data_disclosed_us' => array(
            'step' => STEP_COMPANY,
            'section' => 10,
            'source' => 'wizard',
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __('Select which categories of personal data you have disclosed for a business purpose in the past 12 months', 'complianz-gdpr'),
            'required' => false,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us'),
                'california' => 'yes',
            ),
            //'condition' => array('share_data_other_us' => 'NOT 2'),
            'options' => $this->details_per_purpose_us,
        ),

        'data_sold_us' => array(
            'step' => STEP_COMPANY,
            'section' => 10,
            'source' => 'wizard',
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __('Select which categories of personal data you have sold to Third Parties in the past 12 months', 'complianz-gdpr'),
            'required' => false,
            'callback_condition' => array(
                'california' => 'yes',
                'privacy-statement' => 'generated',
                'regions' => array('us','ca'),
                'purpose_personaldata' => 'selling-data-thirdparty',
            ),
            'condition' => array(),
            'options' => $this->details_per_purpose_us,
        ),

        /*
         * consent boxes
         * */

        'add_consent_to_forms' => array(
            'step' => STEP_COMPANY,
            'section' => 11,
            'source' => 'wizard',
            'type' => 'multicheckbox',
            'required' => false,
            'default' => '',
            'label' => __("For forms detected on your site, you can choose to add a consent checkbox", 'complianz-gdpr'),
            'options' => get_option('cmplz_detected_forms'),
            'callback_condition' => array(
                'contact_processing_data_lawfull'=>'1',
                'regions' => array('eu','uk'),
            ),//when permission is required, add consent box
            'help' => __('You have answered that you use webforms on your site. Not every form that collects personal data requires a checkbox.', 'complianz-gdpr').cmplz_read_more('https://complianz.io/how-to-implement-a-consent-box'),
        ),

        'sensitive_information_processed' => array(
            'step' => STEP_COMPANY,
            'section' => 11,
            'source' => 'wizard',
            'type' => 'radio',
            'required' => false,
            'default' => '',
            'options' => $this->yes_no,
            'label' => __("Does your website contain or process sensitive (personal) information?", 'complianz-gdpr'),
            'callback_condition' => array(
                'regions' => array('ca'),
            ),
            'tooltip' => __('Sensitive personal information is considered to data that is very likely to have a greater impact on Privacy. For example medical, religious or legal information.', 'complianz-gdpr'),
        ),


        //  & SAFETY
        'secure_personal_data' => array(
            'step' => STEP_COMPANY,
            'section' => 11,
            'source' => 'wizard',
            'type' => 'radio',
            'required' => true,
            'default' => '',
            'label' => __("Do you want to provide a detailed list of security measures in your Privacy Statement?", 'complianz-gdpr'),
            'options' => array(
                '1' => __('No, provide a general explanation', 'complianz-gdpr'),
                '2' => __('Yes, we will list our security measures', 'complianz-gdpr')
            ),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
            ),
        ),

        'which_personal_data_secure' => array(
            'step' => STEP_COMPANY,
            'section' => 11,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("Which security measures did you take?", 'complianz-gdpr'),
            'options' => array(
                '1' => __('Username and Password', 'complianz-gdpr'),
                '2' => __('DNSSEC', 'complianz-gdpr'),
                '3' => __('TLS / SSL', 'complianz-gdpr'),
                '4' => __('DKIM, SPF en DMARC', 'complianz-gdpr'),
                '5' => __('Physical security measures of systems which contain personal  data.', 'complianz-gdpr'),
                '6' => __('Security software', 'complianz-gdpr'),
                '7' => __('ISO27001/27002 certified', 'complianz-gdpr'),
                '8' => 'HTTP Strict Transport Security',
                '9' => 'X-Content-Type-Options',
                '10' => 'X-XSS-Protection',
                '11' => 'X-Frame-Options',
                '12' => 'Expect-CT',
                '13' => 'No Referrer When Downgrade header',
                '14' => 'Content Security Policy',
                '15' => __('STARTTLS and DANE','complianz-gdpr'),
                '16' => 'WPA2 Enterprise',
                '17' => 'Permissions Policy',
            ),
            'comment' => sprintf(__("Quickly want to implement most of these security headers? Check out %sReally Simple SSL Pro%s.", "complianz-gdpr"),'<a href="https://really-simple-ssl.com/pro/" target="_blank">','</a>'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
            ),
            'condition' => array('secure_personal_data' => '2'),
        ),

        'financial-incentives' => array(
            'step' => STEP_COMPANY,
            'section' => 12,
            'source' => 'wizard',
            'required' => true,
            'type' => 'radio',
            'default' => '',
            'label' => __("Do you offer financial incentives, including payments to consumers as compensation, for the collection of personal information, the sale of personal information, or the deletion of personal information?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us'),
                'california' => 'yes',
            ),
        ),

        'financial-incentives-terms-url' => array(
            'step' => STEP_COMPANY,
            'section' => 12,
            'placholder' => __('https://your-terms-page.com','complianz-gdpr'),
            'source' => 'wizard',
            'required' => true,
            'type' => 'url',
            'default' => '',
            'label' => __("Enter the URL of the terms & conditions page for the incentives", 'complianz-gdpr'),
            'comment' => sprintf(__('Also see our free %sTerms & Conditions%s plugin', "complianz-gdpr"), '<a href="https://wordpress.org/plugins/complianz-terms-conditions/" target="_blank">', '</a>'),
            'condition' => array('financial-incentives' => 'yes'),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us'),
                'california' => 'yes',
            ),
            'help' => __("Please note that the consumer explicitly has to consent to these terms, and that the consumer must be able to revoke this consent. ", 'complianz-gdpr'),
        ),

        'targets-children' => array(
            'step' => STEP_COMPANY,
            'section' => 13,
            'source' => 'wizard',
            'required' => true,
            'type' => 'radio',
            'default' => '',
            'label' => __("Is your website designed to attract children and/or is it your intent to collect personal data from children under the age of 13?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk', 'ca'),

            ),
        ),

        'children-parent-consent-type' => array(
            'step' => STEP_COMPANY,
            'section' => 13,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("How do you obtain verifiable parental consent for the collection, use, or disclosure of personal information from children?", 'complianz-gdpr'),
            'options' => array(
                'email' => __("We seek a parent or guardian's consent by email",'complianz-gdpr'),
                'creditcard' => __('We seek a high level of consent by asking for a creditcard verification','complianz-gdpr'),
                'phone-chat' => __('We use telephone or Videochat  to talk to the parent or guardian','complianz-gdpr'),
            ),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
            ),
            'condition' => array('targets-children' => 'yes'),
        ),

        'children-safe-harbor' => array(
            'step' => STEP_COMPANY,
            'section' => 13,
            'source' => 'wizard',
            'required' => true,
            'type' => 'radio',
            'default' => '',
            'label' => __("Is your website included in a COPPA Safe Harbor Certification Program?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'us',
            ),
            'condition' => array(
                'targets-children' => 'yes'
            ),
        ),

        'children-name-safe-harbor' => array(
            'step' => STEP_COMPANY,
            'section' => 13,
            'source' => 'wizard',
            'required' => true,
            'type' => 'text',
            'default' => '',
            'label' => __("What is the name of the program?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'us',
            ),
            'condition' => array(
                'children-safe-harbor' => 'yes'
            ),
        ),

        'children-url-safe-harbor' => array(
            'step' => STEP_COMPANY,
            'section' => 13,
            'source' => 'wizard',
            'required' => true,
            'type' => 'url',
            'default' => '',
            'label' => __("What is the URL of the program?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => 'us',
            ),
            'condition' => array(
                'children-safe-harbor' => 'yes'
            ),
        ),

        'children-what-purposes' => array(
            'step' => STEP_COMPANY,
            'section' => 14,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("For what potential activities on your website do you collect personal information from a child?", 'complianz-gdpr'),
            'options' => array(
                'registration' => __('Registration','complianz-gdpr'),
                'content-created-by-child' => __('Content created by a child and publicly shared','complianz-gdpr'),
                'chat' => __('Chat/messageboard','complianz-gdpr'),
                'email' => __('Email contact','complianz-gdpr'),
            ),
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
                'targets-children' => 'yes',
            ),
        ),

        'children-no-safe-harbor-notice' => array(
	        'step' => STEP_COMPANY,
	        'section' => 14,
	        'source' => 'wizard',
	        'required' => false,
	        'type' => 'notice',
	        'default' => '',
	        'help' => sprintf(__("You have indicated that your website is not included in a COPPA Safe Harbor Certification Program. We recommend to check out %sPRIVO%s ,as you target children on your website.", 'complianz-gdpr'),'<a href="https://www.privo.com/" target="_blank">','</a>'),
	        'options' => $this->yes_no,
	        'callback_condition' => array(
		        'privacy-statement' => 'generated',
		        'targets-children' => 'yes',
		        'regions' => 'us',
		        'children-safe-harbor' => 'no',
	        ),
        ),

        'children-what-information-registration' => array(
            'step' => STEP_COMPANY,
            'section' => 14,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("Information collected for registration ", 'complianz-gdpr'),
            'options' => $this->collected_info_children,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
                'targets-children' => 'yes',

            ),
            'condition' => array(
                'children-what-purposes' => 'registration'),
        ),

        'children-what-information-content' => array(
            'step' => STEP_COMPANY,
            'section' => 14,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("Information collected for content created by a child", 'complianz-gdpr'),
            'options' => $this->collected_info_children,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
                'targets-children' => 'yes',
            ),
            'condition' => array(
                'children-what-purposes' => 'content-created-by-child'
            ),
        ),

        'children-what-information-chat' => array(
            'step' => STEP_COMPANY,
            'section' => 14,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("Information collected for chat/messageboard", 'complianz-gdpr'),
            'options' => $this->collected_info_children,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
                'targets-children' => 'yes',

            ),
            'condition' => array(
                'children-what-purposes' => 'chat'
            ),
        ),
        'children-what-information-email' => array(
            'step' => STEP_COMPANY,
            'section' => 14,
            'source' => 'wizard',
            'required' => true,
            'type' => 'multicheckbox',
            'default' => '',
            'label' => __("Information collected for email contact", 'complianz-gdpr'),
            'options' => $this->collected_info_children,
            'callback_condition' => array(
                'privacy-statement' => 'generated',
                'regions' => array('us','uk','ca'),
                'targets-children' => 'yes',

            ),
            'condition' => array(
                'children-what-purposes' => 'email'),
        ),



        //DISCLAIMER
        'themes' => array(
            'step' => STEP_COMPANY,
            'section' => 15,
            'source' => 'wizard',
            'type' => 'multicheckbox',
            'default' => '1',
            'label' => __("Which themes would you like to include in your Disclaimer?", 'complianz-gdpr'),
            'options' => array(
                '1' => __('Liability', 'complianz-gdpr'),
                '2' => __('Reference to terms of use', 'complianz-gdpr'),
                '3' => __('How you will answer inquiries', 'complianz-gdpr'),
                '4' => __('Privacy and reference to the privacy statement', 'complianz-gdpr'),
                '5' => __('Not liable when security is breached', 'complianz-gdpr'),
                '6' => __('Not liable for third-party content', 'complianz-gdpr'),
                '7' => __('Accessibility of the website for the disabled', 'complianz-gdpr'),
            ),
            'callback_condition' => array('disclaimer' => 'generated'),
            'required' => true,
        ),
        'terms_of_use_link' => array(
            'step' => STEP_COMPANY,
            'section' => 15,
            'source' => 'wizard',
            'type' => 'url',
            'default' => '',
            'label' => __("What is the URL of the Terms of Use?", 'complianz-gdpr'),
            'comment' => sprintf(__('Also see our free %sTerms & Conditions%s plugin', "complianz-gdpr"), '<a href="https://wordpress.org/plugins/complianz-terms-conditions" target="_blank">', '</a>'),
            'condition' => array('themes' => '2'),
            'callback_condition' => array('disclaimer' => 'generated'),
            'required' => true,
        ),

        'wcag' => array(
            'step' => STEP_COMPANY,
            'section' => 15,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => 'The WCAG documents explain how to make web content more accessible to people with disabilities.',
            'label' => __("Is your website built according to WCAG 2.1 level AA guidelines?", 'complianz-gdpr'),
            'options' => $this->yes_no,
            'condition' => array('themes' => '7'),
            'callback_condition' => array('disclaimer' => 'generated'),
            'required' => true,
            'help' => cmplz_read_more('https://complianz.io/wcag-2-0-what-is-it/', false),

        ),

        // AUTEURSRECHTEN disclaimer
        'development' => array(
            'step' => STEP_COMPANY,
            'section' => 15,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'label' => __("Who made the content of the website?", 'complianz-gdpr'),
            'options' => array(
                '1' => __('The content is being developed by ourselves', 'complianz-gdpr'),
                '2' => __('The content is being developed or posted by Third Parties', 'complianz-gdpr'),
                '3' => __('The content is being developed by ourselves and other parties', 'complianz-gdpr'),
            ),
            'callback_condition' => array('disclaimer' => 'generated'),
            'required' => true,
        ),

        'ip-claims' => array(
            'step' => STEP_COMPANY,
            'section' => 15,
            'source' => 'wizard',
            'type' => 'radio',
            'default' => '',
            'required' => true,
            'label' => __("What do you want to do with any intellectual property claims?", 'complianz-gdpr'),
            'options' => array(
                '1' => __('All rights reserved', 'complianz-gdpr'),
                '2' => __('No rights reserved', 'complianz-gdpr'),
                '3' => __('Creative Commons - Attribution', 'complianz-gdpr'),
                '4' => __('Creative Commons - Share a like', 'complianz-gdpr'),
                '5' => __('Creative Commons - No derivatives', 'complianz-gdpr'),
                '6' => __('Creative Commons - Noncommercial', 'complianz-gdpr'),
                '7' => __('Creative Commons - Share a like, noncommercial', 'complianz-gdpr'),
            ),
            'callback_condition' => array('disclaimer' => 'generated'),
            'help' => __("Creative Commons (CC) is an American non-profit organization devoted to expanding the range of creative works available for others to build upon legally and to share.", 'complianz-gdpr').cmplz_read_more('https://complianz.io/creative-commons'),
        ),

    );


$this->fields = $this->fields + array(
        'wp_privacy_policies' => array(
	        'label' => __('Plugins', "complianz-gdpr"),
	        'step' => STEP_MENU,
            'section' => 3,
            'source' => 'wizard',
            'type' => 'multiple',
            'callback' => 'wp_privacy_policies',
            'required' => false,
	        'help' => __('Please note that you should customize these texts for your website: the text should generally not be copied as is.', 'complianz-gdpr')
        ),

        'custom_privacy_policy_text' => array(
            'step' => STEP_MENU,
            'section' => 3,
            'translatable' => true,
            'source' => 'wizard',
            'type' => 'editor',
            'label' => __('Annex of your Privacy Statement', "complianz-gdpr"),
            'required' => false,
            'media' => false,
            'callback_condition' => array(
	            'privacy-statement' => 'generated'
            )
        ),
    );
