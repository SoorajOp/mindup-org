<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// define( 'FS_METHOD', 'direct' );
// define( 'FTP_USER', 'atm-op360' );
// define( 'FTP_PASS', 'ATM12345!@#$%' );
// define( 'FTP_HOST', '22' ); //usually localhost:21
// define( 'FTP_SSL','false' ); //if you have ftp with ssl leave it true

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'mindup' );
define( 'DB_NAME', 'mind_up' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7oT6F>f U=ls?(g)`,CUI{g),I_3yhjL!>ew#kwX@54D1eKvc91a7m%h6zz6 +E7');
define('SECURE_AUTH_KEY',  ']E^-TPOFLC80.iq;zagMX[evO-1{UCK=/vaYG#p#G`?Vx9J2O_Vs |m*@bm)NZTJ');
define('LOGGED_IN_KEY',    'C`-uSt+5p x_Vc?6@W-5`5yc&ewts&Lt+9iylXm<Ro3qhPGlb+#ESaJc#vWaP*qM');
define('NONCE_KEY',        '4s}p7w+~i3,ImdIo.US1D1]UP+3gN?aRN#U?zwaG89a`,*h`:%BG4|d@on-|(e-h');
define('AUTH_SALT',        '6VG3u*)q~L;U;cBbC)|p{^mo9Y,,za!.#v1h4Sfe+I`a[QTgbf}z*+a|.6^|(L`s');
define('SECURE_AUTH_SALT', '5YEgURw>%t}j$;R&r?]^20B7%%zc,)lW$E(yMOL4}6@if|.d5bOs1{SEbds`%r[U');
define('LOGGED_IN_SALT',   'M7q=oFMrZkFn>!&Sx*|@HD/>x>Zk=r !YN_0pq0VAS/_ O}.gp&#hAI-|n^KLX@Z');
define('NONCE_SALT',       'JG#b7B;)Zt[-GRy)~(28U97VXG3yG_Aw`X_N2[_qB_GW-F~-Q_ST_Y1C){K[<H;/');



/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wbk8_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);
define( 'DISALLOW_FILE_EDIT', true );
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'WP_DISABLE_FATAL_ERROR_HANDLER', true );
define( 'WPCF7_ADMIN_READ_CAPABILITY', 'manage_options' );
define( 'WPCF7_ADMIN_READ_WRITE_CAPABILITY', 'manage_options' );

// ini_set('display_errors','Off');
// ini_set('error_reporting', E_ALL );
// define('WP_DEBUG_DISPLAY', false);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

